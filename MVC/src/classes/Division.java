package classes;
/**
 * Classe Division
 * @author Panthier Sharon
 * @version 1
 */
public class Division {
	private  String code;
	private  String libelle;
	
	/**
	 * Retourne le code
	 * @return code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * Modifie le code
	 * @param code code de la classe
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * Retourne le libelle
	 * @return libelle
	 */
	public String getLibelle() {
		return libelle;
	}
	/**
	 * Modifie le libelle
	 * @param libelle nom de la classe
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	/**
	 * Constructeur par defaut de la classe division
	 */
	public Division()
	{
		code = "XDFARDI";
		libelle = "Mathématiques";
	}
	/**
	 * Constructeur a 2 parametres
	 * @param code code de la classe
	 * @param libelle nom de la classe
	 */
	public Division(String code,String libelle)
	{
		this.libelle = libelle;
		this.code = code;
	}
	
	@Override
	public String toString()
	{
		return "le code de la classe " +code+" a pour matière "+libelle;
	}
}
