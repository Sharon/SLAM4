package classes;
/**
 * Classe Eleve
 * @author Panthier Sharon
 * @version 1
 */
public class Eleve {
	private String code;
	private String nom;
	private String prenom;
	private String dateNaiss;
	private Division uneDivision;
	
	/**
	 * Retourne le code de l'eleve
	 * @return code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * Modifie le code de l'eleve
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * Retourne le nom de l'eleve
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * Modifie le nom de l'eleve
	 * @param nom nom de l'eleve
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * Retourne le prenom de l'eleve
	 * @return prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * Modifie le prenom de l'eleve
	 * @param prenom prenom de l'eleve
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * Retourne la date de naisance l'eleve
	 * @return date de naissance de l'eleve
	 */
	public String getDateNaiss() {
		return dateNaiss;
	}
	/**
	 * Modifie la date de naisance l'eleve
	 * @param dateNaiss
	 */
	public void setDateNaiss(String dateNaiss) {
		this.dateNaiss = dateNaiss;
	}
	/**
	 * Constructeur par defaut de la classe Eleve
	 */
	public Eleve() {
		code="CPFGEYS";
		nom = "Pierre";
		prenom="Paul-Jacques";
		dateNaiss = "15/02/1998";
		uneDivision = new Division();
	}
	/**
	 * Constructeur a 4 parametre
	 * @param code code de l'eleve
	 * @param nom nom de l'eleve
	 * @param prenom prenom de l'eleve
	 * @param dateNaiss date de naissance de l'eleve
	 */
	public Eleve(String code, String nom, String prenom, String dateNaiss)
	{
		this.code = code;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaiss = dateNaiss;
	}
	/**
	 * Constructeur a 5 parametres
	 * @param code code de l'eleve
	 * @param nom nom de l'eleve
	 * @param prenom prenom de l'eleve
	 * @param dateNaiss date de naissance de l'eleve
	 * @param uneDivision de la Classe division
	 */
	public Eleve(String code, String nom, String prenom, String dateNaiss, Division uneDivision)
	{
		this.code = code;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaiss = dateNaiss;
		this.uneDivision = uneDivision;
	}
	/**
	 * Retourne une division
	 * @return la division,
	 */
	public Division getUneDivision() {
		return uneDivision;
	}
	/**
	 * Retourne une division 
	 * @param uneDivision la division
	 */
	public void setUneDivision(Division uneDivision) {
		this.uneDivision = uneDivision;
	}
	@Override
	public String toString()
	{
		return code +" : L'élève "+nom+" "+prenom+" né le "+dateNaiss;
	}
	
}
