package test;

import java.util.ArrayList;
import java.util.List;

import classes.Eleve;

public class TestEleve {
	public static void main(String[] args) 
	{
		List<Eleve> lesEleves = new ArrayList<>();
		lesEleves.add(new Eleve());
		lesEleves.add(new Eleve("DEFTSGS","Jules","Patrick","22/11/1996"));
		
		for(Eleve e : lesEleves)
		{
			System.out.println(e);
		}
		
		lesEleves.get(0).setDateNaiss("23/11/1995");
		System.out.println("\nModification : date de naissance : "+lesEleves.get(0).getDateNaiss()+" pour l'élève "+lesEleves.get(0).getNom()+ " "+lesEleves.get(0).getPrenom()+"\n");
		
		for(Eleve e : lesEleves)
		{
			System.out.println(e);
		}
	}
}
