package test;

import classes.Division;
import classes.Eleve;
import dao.DAO;
import dao.DivisionDAO;
import dao.EleveDAO;

public class TestDAO {
	public static void main(String[] args) {
	
		DAO<Division> division = new DivisionDAO();

		Division uneDivision = new Division("DFGTE", "Anglais");

		DAO<Eleve> eleve = new EleveDAO();
		// test de la recherche d'un élève en fonction de son code
		System.out.println(eleve.read("DFGHJ").toString());
		Eleve unEleve = new Eleve("TI1DB", "TITI", "TOTO","2008-12-12", uneDivision);
		// test de l'insertion d'un nouvel élève
		eleve.create(unEleve);
		// test de la recherche d'un élève en fonction de son code
		System.out.println(eleve.read("EDRTY").toString());
		System.out.println(eleve.read("TI1DB	").toString());
		// test de la suppression d'un élève
		eleve.delete(unEleve);
		
		// On afficche les informatique de la classe qui a pour code XFSZA
		System.out.println(division.read("XFSZA"));
		division.create(uneDivision);
		System.out.println(division.read("XFPBC"));
		System.out.println(division.read("DFGTE"));
		division.delete(uneDivision);

	}
}
