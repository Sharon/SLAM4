package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import classes.Division;

/***
 * Classe DivisionDAO
 * @author Sharon Panthier
 * @version 1
 * 
 */
public class DivisionDAO extends DAO<Division> {
	/**
	 * Procédure qui insert dans la base de donnée une nouvelle division
	 */
	public void create(Division obj) {
		try {
		 	PreparedStatement prepare = ((Connection) this.connect).prepareStatement("INSERT INTO \"mvc\".division VALUES(?, ?)");
			prepare.setString(1,obj.getCode());
			prepare.setString(2, obj.getLibelle());
					
			prepare.executeUpdate();					
		}
 	        catch (SQLException e) {
		        e.printStackTrace();
		}
	}
	/**
	 * Fonction qui retourne une nouvelle division et qui selectionne toutes les données de la base pour les
	 * affecter a une nouvelle division
	 */
	public Division read(String code) {
		Division laDivision = new Division();
		try {
				Class.forName("org.postgresql.Driver");
		        ResultSet result = ((Connection) this .connect).createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                              ResultSet.CONCUR_UPDATABLE)
                                    .executeQuery("SELECT * FROM \"mvc\".division WHERE code = '" + code +"'");
	           	if(result.first())
	            		laDivision = new Division(code, result.getString("libelle"));   
		} catch (SQLException | ClassNotFoundException e) {
		        e.printStackTrace();
		}
		return laDivision;
	}
	/**
	 * Procédure qui modifie la table division en intégrant le contenu de l'objet Division
	 */
	public void update(Division obj) {
		try {
	
	                ((Connection) this .connect).createStatement(
	                    	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                        ResultSet.CONCUR_UPDATABLE
	                     ).executeUpdate(
	                    	"UPDATE \"mvc\".division SET libelle = '" + obj.getLibelle() + "'"+
	                    	" WHERE code = '" + obj.getCode()+"'"
	                     );
		    } catch (SQLException e) {
		            e.printStackTrace();
		    }
		}

	/**
	 * Procédure qui supprime une Divsion en fonction de son code
	 */
	public void delete(Division obj) {
		try {
	                ((Connection) this.connect).createStatement(
	                             ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                             ResultSet.CONCUR_UPDATABLE
	                        ).executeUpdate(
	                             "DELETE FROM \"mvc\".division WHERE code = '" + obj.getCode()+"'"
	                        );
				
	        } catch (SQLException e) {
		            e.printStackTrace();
		    }
		}
}
