package dao;

import java.sql.Connection;

/**
 * Classe abstraite DAO
 * @author Panthier Sharon
 *	@version 1
 * @param <T> Designe n'importe qu'elle classe
 */
public abstract class DAO<T>{
	public Connection connect =  ConnexionPostgreSql.getInstance();
	
	/**
	 * Procedure qui creer un objet en fonction de la classe
	 * @param obj la classe a creer 
	 */
	public abstract void create(T obj);
	/**
	 * Fonction qui lit le code
	 * @param code code a lire
	 * @return 
	 */
	public abstract T read(String code);
	/**
	 * Procedure qui modifie la classe
	 * @param obj la classe a modifier
	 */
	public abstract void update(T obj);
	/**
	 * Procedure qui supprime la classe
	 * @param obj la classe a supprimer
	 */
	public abstract void delete(T obj);

}
