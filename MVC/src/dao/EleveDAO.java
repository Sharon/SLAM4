package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import classes.Eleve;

/***
 * Classe EleveDAO
 * @author Sharon Panthier
 * @version 1
 * 
 */
public class EleveDAO extends DAO<Eleve>{
	/**
	 * Procédure qui insert dans la base de donnée une nouvelle eleve
	 */
	@Override
	public void create(Eleve obj) {
		try {
		 	PreparedStatement prepare = ((Connection) this.connect).prepareStatement("INSERT INTO \"mvc\".eleve VALUES(?, ?, ?, ?)");
			prepare.setString(1,obj.getCode());
			prepare.setString(2, obj.getNom());
			prepare.setString(3, obj.getPrenom());
			prepare.setString(4, obj.getDateNaiss());
					
			prepare.executeUpdate();					
		}
 	        catch (SQLException e) {
		        e.printStackTrace();
		}
		
	}
	/**
	 * Fonction qui retourne un nouveau eleve et qui selectionne toutes les données de la base pour les
	 * affecter a un nouveau eleve
	 */
	@Override
	public Eleve read(String code) {
		Eleve unEleve = new Eleve();
		try {
			Class.forName("org.postgresql.Driver");
		        ResultSet result = ((Connection) this .connect).createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                              ResultSet.CONCUR_UPDATABLE)
                                    .executeQuery("SELECT * FROM \"mvc\".eleve WHERE code = '" + code +"'");
	           	if(result.first())
	           		unEleve = new Eleve(code, result.getString("nom"),result.getString("prenom"), result.getString("dateNaiss"));   
		} catch (SQLException | ClassNotFoundException e) {
		        e.printStackTrace();
		}
		return unEleve;
	}
	/**
	 * Procédure qui modifie la table division en intégrant le contenu de l'objet Eleve
	 */
	@Override
	public void update(Eleve obj) {
		try {
			
            ((Connection) this .connect).createStatement(
                	ResultSet.TYPE_SCROLL_INSENSITIVE, 
                    ResultSet.CONCUR_UPDATABLE
                 ).executeUpdate(
                	"UPDATE \"mvc\".eleve SET nom = '" + obj.getNom() + "', prenom = '"+obj.getPrenom()+"', dateNaiss ='"+obj.getDateNaiss()+"'"+
                	" WHERE code = '" + obj.getCode()+"'"
                 );
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }
			
	}
	/**
	 * Procédure qui supprime un Eleve dans la table 'mvc' en fonction de son code
	 */
	@Override
	public void delete(Eleve obj) {
		try {
            ((Connection) this.connect).createStatement(
                         ResultSet.TYPE_SCROLL_INSENSITIVE, 
                         ResultSet.CONCUR_UPDATABLE
                    ).executeUpdate(
                         "DELETE FROM \"mvc\".eleve WHERE code = '" + obj.getCode()+"'"
                    );
		
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }
		
	}
	

}
