package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionPostgreSql {
	// Création d'une nouvelle connexion
	static Connection connect;
	public static Connection getInstance()
	{
		// Tentative de connexion a la base de données
		if(connect == null) {
			try {
				String url = "jdbc:postgresql://postgresql.bts-malraux72.net/spanthier";
				connect = DriverManager.getConnection(url,"s.panthier", "P@ssword");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return connect;
	}
}
