package vues;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class ViewMVC extends JFrame implements ActionListener, WindowListener{
	static JPanel panel = new JPanel();
	static JMenu fermer = new JMenu("Fermer");
	
	public ViewMVC() {
		super("Nos cher élèves ! ");
		this.addWindowListener(this);
		build();
	}
	public void build()
	{
		// JMenuBar
		JMenuBar menuBar = new JMenuBar();

		
		// JMenu
		JMenu menuDivision = new JMenu("Division");
		JMenu menuEleve = new JMenu("Eleves");
		
		fermer.addActionListener(this);
		
		// Utiliser Alt + <lettre> pour changer de Jmenu
		menuEleve.setMnemonic('E');
		menuDivision.setMnemonic('D');
		
		// JMenuItem
		JMenuItem itmvisualiser = new JMenuItem("Visualiser");
		JMenuItem itmmodifier = new JMenuItem("Modifier");
		JMenuItem itmajouter = new JMenuItem("Ajouter");
		JMenuItem itmsupprimer = new JMenuItem("Supprimer");
		
		menuDivision.add(itmvisualiser);
		menuDivision.add(itmmodifier);
		menuDivision.add(itmajouter);
		menuDivision.add(itmsupprimer);
		
		
		menuBar.add(menuDivision);
		menuBar.add(menuEleve);
		menuBar.add(fermer);
		
		
		panel.add(menuBar);		
				
		panel.setBackground(Color.WHITE);
		
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setSize(500, 500);
        setVisible(true);
        setContentPane(panel);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==fermer) {
			System.exit(0);
			
		}
	}
	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
}
