<div data-role="page" id="pagegestionoffres">
    <?php
    include "vues/enteteavecretour.html";
    ?>
    <div data-role="content" id="divgestion">  
        <h1>Gérer mes offres</h1>
        <legend>Mes offres au départ de l'entreprise</legend>
        <form>
            <fieldset data-role="controlgroup">
                <?php
                foreach ($lesOffresDepart as $depart) {
                    if ($depart['idchauffeur'] == $_SESSION['id']) {
                        ?>
                        <input name="<?php echo $depart['id'] ?>" id="<?php echo $depart['id'] ?>" type="checkbox">
                        <label for="<?php echo $depart['id'] ?>"><?php echo $depart['jour'] . " " . $depart['date'] . " à " . $depart['heure'] ?></label>

                        <?php
                    }
                }//Fin foreach
                ?>
            </fieldset>
        </form>
        <legend>Mes offres au arrivée de l'entreprise</legend>
        <form>
            <fieldset data-role="controlgroup">
                <?php
                foreach ($lesOffresArrivee as $arrivee) {
                    if ($arrivee['idchauffeur'] == $_SESSION['id']) {
                        ?>
                        <input name="<?php echo $arrivee['id'] ?>" id="<?php echo $arrivee['id'] ?>" type="checkbox">
                        <label for="<?php echo $arrivee['id'] ?>"><?php echo $arrivee['jour'] . " " . $arrivee['date'] . " à " . $arrivee['heure'] ?></label>
                        <?php
                    }
                }
                ?>
            </fieldset>
        </form>
        <a href="#" class="ui-btn ui-icon-delete ui-btn-icon-left" id="btnsupprimer">Supprimer</a>
    </div> <!-- /content -->
    <?php
    include "vues/pied.html";

    ?>
</div> <!-- /page -->