<div data-role="page" id="pageoffresoffertes">
    <?php
    include "vues/enteteavecretour.html";
    ?>
    <div data-role="content" id="divliste"> 
        <div data-role="collapsibleset" data-theme="b" data-content-theme="a">

            <?php
            $jour = "";
            $i = 0;
            foreach ($lesOffres as $uneOffre) {
                if ($jour != $uneOffre['jour']) {
                    $jour = $uneOffre['jour'];
                    if ($i != 0) {
                        ?> </ul>
                    </div>
                    <?php
                }
                $i++;
                ?>

                <div data-role="collapsible">
                    <h3><?php echo $jour ?></h3>
                    <ul id="lsOffres" data-role="listview">
                        <?php
                    } //fin si
                    $line = $uneOffre['date']." à ". $uneOffre['heure'];
                    if ($_SESSION['gestion_offre'] == "depart") {
                        $line .=  " départ de ".$uneOffre['retour'];
                    } else {
                        $line .= " arrivée a ".$uneOffre['depart'];
                    }
                    ?>
                        <li id="<?php echo $uneOffre['id']; ?>"><a href ="#pageoffre" ><?php echo $line;?></a></li>
                    <?php
                } //fin foreach
                ?>
            </ul>
        </div>
    </div>
</div><!-- /content -->
<?php
include "vues/pied.html";
?>
</div> <!-- /page -->
