<?php
session_start();
require_once 'util/fonctions.php';
include "vues/entete.html";

if (!isset($_REQUEST['action'])) {
    $action = 'accueil';
} else {
    $action = $_REQUEST['action'];
}

switch ($action) {
    case 'accueil':
        include "vues/pageconnexion.php";
        include "vues/pagemenuaccueil.php";
        break;
    case 'inscription':
        include "vues/pageinscription.php";
        break;
    case 'gereroffresdepartentreprise':
        $lesOffres = getLesOffresDepartEntreprise();
        if ($_SESSION['login'] != null) {
            $_SESSION['gestion_offre'] = "depart";
            include "vues/pageoffresoffertes.php";
            include "vues/pageoffre.php";
        } else {
            include "vues/pageconnexion.php";
            include "vues/pagemenuaccueil.php";
        }
        break;
    case 'gereroffresarriveeentreprise':
        $lesOffres = getLesOffresArriveeEntreprise();
        $_SESSION['gestion_offre'] = "arrivee";
        include "vues/pageoffresoffertes.php";
        include "vues/pageoffre.php";
        break;
    case 'gereroffres':
        $id = $_SESSION['id'];
        $lesOffresDepart = getLesOffresDepartEntreprise();
        $lesOffresArrivee = getLesOffresArriveeEntreprise();
        include 'vues/pagegestionoffre.php';
        break;
}
?>
</body>
</html>
