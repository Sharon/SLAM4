$(function () {
    /*********************************** Page connexion************************************/

    $('#btnconnexion').click(function (e) {
        // les deux lignes annulent le comportement par défaut du clic
        // c'est à dire href="#" qui provoquerait un rappel de la même page
        e.preventDefault();
        e.stopPropagation();
        var mdp = $("#mdp").val(); //récupère le contenue de la zone d'id mdp
        var login = $("#login").val();

        /* appel d'une fonction ajax -> post*/
        // elle prend 3 arguments :
        // 1- le nom de la fonction php qui sera exécutée
        // 2- la liste des arguments à fournir à cette fonction
        // 3- le nom de la fonction JS qui sera exécutée au "retour" du serveur 
        $.post("ajax/traiterconnexion.php", {
            // valorise les deux arguments passés à la fonction traiterconnexion.php
            "mdp": mdp,
            "login": login},
                foncRetourConnexion);
    });

    /* fonction JS qui sera exécutée après le ret{our de l'appel ajax précedent (traiterconnexion.php) */
    // le paramètre data représente la donnée envoyée par le serveur
    // résultat de l'appel de la fonction retourConnexion.php
    function foncRetourConnexion(data) {
        if (data.length !== 0) {
            // charge la page (data-role=page) du même document dont l'id  est le sélecteur indiqué
            //On cache la div de connexion lorsque l'utilisateur est connecté
            $.mobile.changePage("#pagemenuaccueil");
            $("#divconnexion").hide();


        } else {
            // sinon affichage d'un message dans la div d'id message  
            var styles = {
                color: "red",
                "text-align": "center"
            };
            $("#message").css(styles);
            $("#message").html("erreur de login et/ou mdp");
        }
    }

    // Si on  clique sur la liste alors on récupère l(i du chauffeur
    $("#lsOffres > li").click(function () {
        var id = $(this).attr("id");
        $.post("ajax/traiteroffre.php", {
            "id": id
        },
                foncAfficherOffre, "json");
    });


    // Affiche les information des co-voitureurs
    function foncAfficherOffre(data) {

        $("#nom").html(data["nom"]);
        $("#prenom").html(data["prenom"]);
        $("#mail").attr("href", "mailto:" + data["mail"]);
        $("#tel").attr("href", "tel:" + data["tel"]);
        // On verifie si il y a des étapes
        if (data["ramassage"]) {
            var txt = "<br>Etapes possibles : <br><ul>";
            var tabRamassage = data["ramassage"];
            for (var etape in tabRamassage) { // On part le tablea qui contient les étapes
                txt += "<li>" + tabRamassage[etape]['lieu'] + "</li>";
            }
            txt += "</ul>";
            $("#ramassage").html(txt);
        }
    }
    /*********************************** Page gestion************************************/
    $('#btngestion').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        
    });

    /***************************************** Page inscription *******************************/

    $('#btninscription').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var nom = $("#nom").val();
        var prenom = $("#prenom").val();
        var mail = $("#mail").val();
        var tel = $("#tel").val();
        $.post("ajax/enregistreruser.php", {
            "nom": nom,
            "prenom": prenom,
            "mail": mail,
            "tel": tel,
            "type": $("input[type=radio][name=service]:checked").attr("value")},
                foncRetourEnregistrement);

    });
    function foncRetourEnregistrement(data) {
        $("#divinscription").html(data);

    }



}); // fin fonction principale/* 


