<?php

// code non encore écrit qui retournera le login s'il est présent en base ou une chaine vide, 
// à faire dans une itération suivante
function verifuser($login, $mdp) {
    $lesusers = GetLesUsers();
    $templog = "";
    foreach ($lesusers as $usr) {
        if ($usr['login'] == $login && $usr['mdp']==$mdp) {
           $templog =  $usr['id'];
        } 
    }
    return $templog;

}

/* retourne la liste des inscrits */

function GetLesUsers() {
    $tab = array(
        array("id" => "c1", "login" => "pdurand", "mdp" => "abcd12"),
        array("id" => "c2", "login" => "amarquis", "mdp" => "efg543"),
        array("id" => "c3", "login" => "mlabourot", "mdp" => "xyz987"));
    return $tab;
}

// fonction qui retourne un mot de passe aléatoire sur 6 caractères
function motPasse() {
    $longueurDuMotDePasse = 6;
    $caracteres = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
    $carAleatoires = array_rand($caracteres, $longueurDuMotDePasse);
    $pass = "";
    foreach ($carAleatoires as $i) {
        $pass .= $caracteres[$i];
    }
    return $pass;
}

// retourne un tableau trié sur les jours de la semaine croissants
function getLesOffresDepartEntreprise() {
    $tab = array(
        array("id" => 1,
            "idchauffeur" => "c1",
            "jour" => "mardi",
            "date" => "permanent",
            "heure" => "15h",
            "retour" => "paris 20",
            "nom" => "Durand",
            "prenom" => "Pierre",
            "mail" => "pierre.durand@societe.com",
            "tel" => "0148592370"),
        array("id" => 2,
            "idchauffeur" => "c2",
            "jour" => "mardi",
            "date" => "21/11/2017",
            "heure" => "16h",
            "retour" => "Métro Robespierre",
            "nom" => "Marquis",
            "prenom" => "Aurore",
            "mail" => "aurore.marquis@societe.com",
            "tel" => "0688300169"),
        array("id" => 3,
            "idchauffeur" => "c3",
            "jour" => "mercredi",
            "date" => "22/11/2017",
            "heure" => "17h30",
            "retour" => "Bobigny Mairie",
            "nom" => "Labourot",
            "prenom" => "Manon",
            "mail" => "manon.labourot@societe.com",
            "tel" => "0145879823"),
        array("id" => 4,
            "idchauffeur" => "c1",
            "jour" => "mercredi",
            "date" => "permanent",
            "heure" => "17h",
            "retour" => "paris 20",
            "nom" => "Durand",
            "prenom" => "Pierre",
            "mail" => "pierre.durand@societe.com",
            "tel" => "0148592370"),
        array("id" => 5,
            "idchauffeur" => "c2",
            "jour" => "vendredi",
            "date" => "24/11/2017",
            "heure" => "17h30",
            "retour" => "Métro Robespierre",
            "nom" => "Marquis",
            "prenom" => "Aurore",
            "mail" => "aurore.marquis@societe.com",
            "tel" => "0688300169"),
        array("id" => 6,
            "idchauffeur" => "c3",
            "jour" => "vendredi",
            "date" => "permanent",
            "heure" => "15h30",
            "retour" => "Bobigny Mairie",
            "nom" => "Labourot",
            "prenom" => "Manon",
            "mail" => "manon.labourot@societe.com",
            "tel" => "0145879823"),
    );
    return $tab;
}

// Recherche les infos sur le conducteur et le stock dans un tableau
function SearchInfo($id, $nbchoix) {

// Si le choix est 0 alors on est dans les offres de départ
    if ($nbchoix == 0) {
// On récupère toutes les offres de depart
        $tab = getLesOffresDepartEntreprise();

        $infos = array();
// On parcourt le tableau
        foreach ($tab as $value) {
// Si l'id est correct alors on l'affecte a un nouveau tableau
            if ($value["id"] == $id) {
                $infos["nom"] = $value["nom"];
                $infos["prenom"] = $value["prenom"];
                $infos["mail"] = $value["mail"];
                $infos["tel"] = $value["tel"];
            }
        }
// SInon si le nombre choisi est 1 alors on est dans les offres de depart
    } else if ($nbchoix == 1) {
        $tab = getLesOffresArriveeEntreprise();

        $infos = array();
        foreach ($tab as $value) {
            if ($value["id"] == $id) {
                $infos["nom"] = $value["nom"];
                $infos["prenom"] = $value["prenom"];
                $infos["mail"] = $value["mail"];
                $infos["tel"] = $value["tel"];
                $infos["ramassage"] = $value["ramassage"];
            }
        }
    }
// On retourne le nouveau tableau affecté 
    return $infos;
}

function getLesOffresArriveeEntreprise() {
    $tab = array(
        array(
            "id" => 7,
            "idchauffeur" => "c1",
            "jour" => "lundi",
            "date" => "permanent",
            "heure" => "8h",
            "ramassage" => array(array("id" => 1, "lieu" => "porte des lilas"),
                array("id" => 2, "lieu" => "porte de bagnolet")),
            "depart" => "paris 20",
            "nom" => "Durand",
            "prenom" => "Pierre",
            "mail" => "pierre.durand@societe.com",
            "tel" => "0148592370"
        ),
        array(
            "id" => 8,
            "idchauffeur" => "c1",
            "jour" => "mardi",
            "date" => "21/11/2017",
            "heure" => "9h",
            "ramassage" => array(array("id" => 1, "lieu" => "porte des lilas"),
                array("id" => 2, "lieu" => "porte de bagnolet")),
            "depart" => "paris 20",
            "nom" => "Durand",
            "prenom" => "Pierre",
            "mail" => "pierre.durand@societe.com",
            "tel" => "0148592370"),
        array("id" => 9, "idchauffeur" => "c2", "jour" => "mardi",
            "date" => "21/11/2017", "heure" => "7h30",
            "ramassage" =>
            array(array("id" => 1, "lieu" => "Mairie de Montreuil"),
                array
                    ("id" => 2, "lieu" => "A3 Villiers")),
            "depart" => "Métro Robespierre", "nom" => "Marquis",
            "prenom" => "Aurore",
            "mail" => "aurore.marquis@societe.com",
            "tel" => "0688300169"),
        array("id" => 10, "idchauffeur" => "c2", "jour" => "mercredi",
            "date" => "permanent", "heure" => "8h",
            "ramassage" =>
            array(array("id" => 1, "lieu" => "Mairie de Montreuil"),
                array
                    ("id" => 2, "lieu" => "A3 Villiers")),
            "depart" => "Métro Robespierre", "nom" => "Marquis",
            "prenom" => "Aurore",
            "mail" => "aurore.marquis@societe.com",
            "tel" => "0688300169"),
        array("id" => 11, "idchauffeur" => "c1", "jour" => "vendredi",
            "date" => "24/11/2017", "heure" => "7h30",
            "ramassage" => array(array("id" => 1, "lieu" => "porte des lilas"),
                array("id" => 2, "lieu" => "porte de bagnolet")),
            "depart" => "Paris 20", "nom" => "Durand",
            "prenom" => "Pierre",
            "mail" => "pierre.durand@societe.com", "tel" => "0148592370"),
        array("id" => 12, "idchauffeur" => "c3", "jour" => "vendredi",
            "date" => "permanent", "heure" => "8h",
            "ramassage" => array(array("id" => 1, "lieu" => "Bobigny A3"),
                array("id" => 2, "lieu" => "Bondy")),
            "depart" => "Bobigny Mairie", "nom" => "Labourot",
            "prenom" => "Manon",
            "mail" => "manon.labourot@societe.com", "tel" => "0145879823"),
    );
    return $tab;
}

?>
