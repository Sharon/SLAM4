<?php

/**
 * @Entity @Table(name="instrument")
 * */
class Instrument {

    /**
     * @Id @Column(type="string", length=5)  
     * */
    private $reference;

    /**
     * @Column(type="string", length=30)
     * @var string
     * */
    private $nom;

    /**
     * @Column(type="string", length=20)
     * @var string
     * */
    private $marque;

    /**
     * @Column(type="string", length=150)
     * @var string
     * */
    private $caracteristiq;

    /**
     * @Column(type="integer")
     * */
    private $prix;

    /**
     * @Column(type="string", length=20)
     * @var string
     * */
    private $photo;

    /**
     * @ManyToOne(targetEntity="categorie")
     * @JoinColumn (name="codecateg",referencedColumnName="codecateg")
     * */
    private $codecateg;

// *** Le constructeur ainsi que les getteurs et setteurs  
    public function init($reference, $nom, $marque, $caracteristiq, $prix, $photo, $categorie) {
        $this->reference = $reference;
        $this->nom = $nom;
        $this->marque = $marque;
        $this->caracteristiq = $caracteristiq;
        $this->prix = $prix;
        $this->photo = $photo;
        $this->codecateg = $categorie;
    }

    public function getReference() {
        return $this->reference;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getMarque() {
        return $this->marque;
    }

    public function getCaracteristiq() {
        return $this->caracteristiq;
    }

    public function getPrix() {
        return $this->prix;
    }

    public function getPhoto() {
        return $this->photo;
    }

    public function getCodeCateg() {
        return $this->codecateg;
    }

}
