<?php

/**
 * @Entity @Table(name="categorie")
 * */
class Categorie {

    /**
     * @Id @Column(type="string", length=2) 
     * @var code de la catégorie
     * */
    private $codecateg;

    /**
     * @Column(type="string", length=30)
     * @var nom de la catégorie
     * */
    private $nomcateg;

// *** Le constructeur ainsi que les getteurs et setteurs 
    public function init($codecateg, $nomcateg) {
        $this->codecateg = $codecateg;
        $this->nomcateg = $nomcateg;
    }

    public function getCodecateg() {
        return $this->codecateg;
    }

    public function getNomcateg() {
        return $this->nomcateg;
    }
    
    public function setCodecateg($codecateg) {
        $this->codecateg = $codecateg;
    }
    public function setNomcateg($nomcateg) {
        $this->nomcateg = $nomcateg;
    }
}

?>
