<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

/* Il faut activer une des 3 lignes suivantes en fonction du langage (PHP, XML ou YAML) 
 * que l'on va utiliser pour coder les classes permettant d'accéder à la base de données.
 * 
 * Dans ces 3 lignes, le paramètre des méthodes createAnnotationMetadataConfiguration,
 * createXMLMetadataConfiguration, et createYAMLMetadataConfiguration spécifie le répertoire 
 * dans lequel sont codées les classes pour l'accès à la base de données.
 * le chemin indiqué dans ces 3 lignes correspond à l'arborescence qui a été créée précédemment.
 */

$config = Setup::createAnnotationMetadataConfiguration([__DIR__."/src"], true);
//$config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), true);
//$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), true);
$connexion = array(
'driver' => 'pdo_mysql',
'host' => '80.82.238.198',
'port' => '3306',
'dbname' => 's.panthier',
'user' => 's.panthier',
'password' => 'passe',
);
// obtention de l'entity manager
$entityManager = EntityManager::create($connexion, $config);
