<?php

/* cinemaconsultationBundle:Default:unstudio.html.twig */
class __TwigTemplate_ce400599999be802fcd840b929e69e86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "affiche un studio
";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "
    <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_homepage"), "html", null, true);
        echo "\">
Accueil
</a>
    
    <h1> Titre : ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unstudio"]) ? $context["unstudio"] : $this->getContext($context, "unstudio")), "nom"), "html", null, true);
        echo "</h1>
    <p>adresse: ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unstudio"]) ? $context["unstudio"] : $this->getContext($context, "unstudio")), "adresse"), "html", null, true);
        echo "</p>
    <p> Pays : ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unstudio"]) ? $context["unstudio"] : $this->getContext($context, "unstudio")), "pays"), "html", null, true);
        echo "</p>
    <p> CA: ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unstudio"]) ? $context["unstudio"] : $this->getContext($context, "unstudio")), "ca"), "html", null, true);
        echo " yen </p>
    
";
    }

    public function getTemplateName()
    {
        return "cinemaconsultationBundle:Default:unstudio.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 15,  58 => 14,  54 => 13,  50 => 12,  43 => 8,  40 => 7,  37 => 6,  32 => 3,  29 => 2,);
    }
}
