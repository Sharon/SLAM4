<?php

/* cinemaconsultationBundle:Default:index1.html.twig */
class __TwigTemplate_a34765e3ffdeb95efd1cbab872e28aee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    Acceuil
";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "    <h1> Bienvenue </h1>
    <h3>Film</h3>
    <ul>
        ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["film"]) ? $context["film"] : $this->getContext($context, "film")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["unFilm"]) {
            // line 11
            echo "            <a href =\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_affichefilm", array("id" => $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "id"))), "html", null, true);
            echo "\">
                <li>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "title"), "html", null, true);
            echo "</li>
            </a>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 15
            echo "            Aucun Film !!
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['unFilm'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 17
        echo "    </ul>

    <h3>Studio</h3>
    <ul>
        ";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["studio"]) ? $context["studio"] : $this->getContext($context, "studio")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["unstudio"]) {
            // line 22
            echo "            <a href =\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_affichestudio", array("id" => $this->getAttribute((isset($context["unstudio"]) ? $context["unstudio"] : $this->getContext($context, "unstudio")), "id"))), "html", null, true);
            echo "\">
                <li>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unstudio"]) ? $context["unstudio"] : $this->getContext($context, "unstudio")), "nom"), "html", null, true);
            echo "</li>
            </a>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 26
            echo "            Aucun studio !!
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['unstudio'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 28
        echo "
    </ul>
";
    }

    public function getTemplateName()
    {
        return "cinemaconsultationBundle:Default:index1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 28,  94 => 26,  86 => 23,  81 => 22,  76 => 21,  70 => 17,  63 => 15,  55 => 12,  50 => 11,  45 => 10,  40 => 7,  37 => 6,  32 => 3,  29 => 2,);
    }
}
