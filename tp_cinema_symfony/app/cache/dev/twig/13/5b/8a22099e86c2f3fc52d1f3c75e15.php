<?php

/* cinemaconsultationBundle:studio:affichefilmetstudio.html.twig */
class __TwigTemplate_135b8a22099e86c2f3fc52d1f3c75e15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    les films
";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["film"]) ? $context["film"] : $this->getContext($context, "film")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["unFilm"]) {
            // line 8
            echo "    <h1> Titre : ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "title"), "html", null, true);
            echo "</h1>
    <p>Resume : ";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "resume"), "html", null, true);
            echo "</p>
    <p>Duree : ";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "duree"), "html", null, true);
            echo " minutes</p>
    <p> Pays : ";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "pays"), "html", null, true);
            echo "</p>
    <p> Recette : ";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "recette"), "html", null, true);
            echo " yen </p>
    
";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 15
            echo "    Erreur
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['unFilm'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
    }

    public function getTemplateName()
    {
        return "cinemaconsultationBundle:studio:affichefilmetstudio.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 15,  62 => 12,  58 => 11,  54 => 10,  50 => 9,  45 => 8,  40 => 7,  37 => 6,  32 => 3,  29 => 2,);
    }
}
