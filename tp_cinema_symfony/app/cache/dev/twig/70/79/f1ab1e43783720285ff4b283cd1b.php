<?php

/* cinemaconsultationBundle:Default:unfilm.html.twig */
class __TwigTemplate_7079f1ab1e43783720285ff4b283cd1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "affiche un films
";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "
    <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_homepage"), "html", null, true);
        echo "\">
Accueil
</a>
    
    <h1> Titre : ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "title"), "html", null, true);
        echo "</h1>
    <p>Resume : ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "resume"), "html", null, true);
        echo "</p>
    <p>Duree : ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "duree"), "html", null, true);
        echo " minutes</p>
    <p> Pays : ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "pays"), "html", null, true);
        echo "</p>
    <p> Recette : ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "recette"), "html", null, true);
        echo " yen </p>
    
";
    }

    public function getTemplateName()
    {
        return "cinemaconsultationBundle:Default:unfilm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 16,  62 => 15,  58 => 14,  54 => 13,  50 => 12,  43 => 8,  40 => 7,  37 => 6,  32 => 3,  29 => 2,);
    }
}
