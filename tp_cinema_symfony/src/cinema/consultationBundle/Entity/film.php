<?php

namespace cinema\consultationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * film
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class film
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=100)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="duree", type="string", length=100)
     */
    private $duree;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=100)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="resume", type="string", length=100)
     */
    private $resume;

    /**
     * @var string
     *
     * @ORM\Column(name="recette", type="string", length=100)
     */
    private $recette;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return film
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set duree
     *
     * @param string $duree
     * @return film
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;
    
        return $this;
    }

    /**
     * Get duree
     *
     * @return string 
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set pays
     *
     * @param string $pays
     * @return film
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    
        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set resume
     *
     * @param string $resume
     * @return film
     */
    public function setResume($resume)
    {
        $this->resume = $resume;
    
        return $this;
    }

    /**
     * Get resume
     *
     * @return string 
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * Set recette
     *
     * @param string $recette
     * @return film
     */
    public function setRecette($recette)
    {
        $this->recette = $recette;
    
        return $this;
    }

    /**
     * Get recette
     *
     * @return string 
     */
    public function getRecette()
    {
        return $this->recette;
    }
}
