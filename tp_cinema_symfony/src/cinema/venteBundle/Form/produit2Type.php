<?php

namespace cinema\venteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class produit2Type extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text')
                ->add('description', 'text')
                ->add('prixHT', 'text')
                ->add('famille', 'entity', array(
                    'label' => 'Famille',
                    'class' => 'cinemaventeBundle:famille2',
                    'property' => 'libelle',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'cinema\venteBundle\Entity\produit2'
        ));
    }

    public function getName() {
        return 'cinema_ventebundle_produit2type';
    }

}
