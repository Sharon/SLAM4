<?php

namespace cinema\venteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * produit2
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class produit2
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="prixHT", type="decimal", scale=2)
     */
    private $prixHT;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="famille2")
     * @ORM\JoinColumn(name="idFamille", referencedColumnName="id")
     */
    private $famille;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return produit2
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return produit2
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prixHT
     *
     * @param float $prixHT
     * @return produit2
     */
    public function setPrixHT($prixHT)
    {
        $this->prixHT = $prixHT;
    
        return $this;
    }

    /**
     * Get prixHT
     *
     * @return float 
     */
    public function getPrixHT()
    {
        return $this->prixHT;
    }

    /**
     * Set categorie
     *
     * @param integer $categorie
     * @return produit2
     */
    public function setFamille($famille)
    {
        $this->famille = $famille;
    
        return $this;
    }

    /**
     * Get categorie
     *
     * @return integer 
     */
    public function getFamille()
    {
        return $this->famille;
    }
}
