<?php

namespace cinema\venteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use cinema\venteBundle\Entity\produit2;
use cinema\venteBundle\Form\produit2Type;

/**
 * produit2 controller.
 *
 */
class produit2Controller extends Controller
{
    /**
     * Lists all produit2 entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('cinemaventeBundle:produit2')->findAll();

        return $this->render('cinemaventeBundle:produit2:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new produit2 entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new produit2();
        $form = $this->createForm(new produit2Type(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('produit2_show', array('id' => $entity->getId())));
        }

        return $this->render('cinemaventeBundle:produit2:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new produit2 entity.
     *
     */
    public function newAction()
    {
        $entity = new produit2();
        $form   = $this->createForm(new produit2Type(), $entity);

        return $this->render('cinemaventeBundle:produit2:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a produit2 entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:produit2')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find produit2 entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('cinemaventeBundle:produit2:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing produit2 entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:produit2')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find produit2 entity.');
        }

        $editForm = $this->createForm(new produit2Type(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('cinemaventeBundle:produit2:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing produit2 entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:produit2')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find produit2 entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new produit2Type(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('produit2_edit', array('id' => $id)));
        }

        return $this->render('cinemaventeBundle:produit2:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a produit2 entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('cinemaventeBundle:produit2')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find produit2 entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('produit2'));
    }

    /**
     * Creates a form to delete a produit2 entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
