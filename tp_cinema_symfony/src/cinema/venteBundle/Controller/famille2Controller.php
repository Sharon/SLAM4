<?php

namespace cinema\venteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use cinema\venteBundle\Entity\famille2;
use cinema\venteBundle\Form\famille2Type;

/**
 * famille2 controller.
 *
 */
class famille2Controller extends Controller
{
    /**
     * Lists all famille2 entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('cinemaventeBundle:famille2')->findAll();

        return $this->render('cinemaventeBundle:famille2:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new famille2 entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new famille2();
        $form = $this->createForm(new famille2Type(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('famille2_show', array('id' => $entity->getId())));
        }

        return $this->render('cinemaventeBundle:famille2:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new famille2 entity.
     *
     */
    public function newAction()
    {
        $entity = new famille2();
        $form   = $this->createForm(new famille2Type(), $entity);

        return $this->render('cinemaventeBundle:famille2:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a famille2 entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:famille2')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find famille2 entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('cinemaventeBundle:famille2:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing famille2 entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:famille2')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find famille2 entity.');
        }

        $editForm = $this->createForm(new famille2Type(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('cinemaventeBundle:famille2:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing famille2 entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:famille2')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find famille2 entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new famille2Type(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('famille2_edit', array('id' => $id)));
        }

        return $this->render('cinemaventeBundle:famille2:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a famille2 entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('cinemaventeBundle:famille2')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find famille2 entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('famille2'));
    }

    /**
     * Creates a form to delete a famille2 entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
