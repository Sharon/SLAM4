<?php

namespace cinema\venteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('cinemaventeBundle:famille2')->findAll();

        return $this->render('cinemaventeBundle:famille2:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

}
