# SLAM4 - Réalisation et maintenance de composants logiciels 

Ce module aborde les savoirs et savoir-faire liés à la mise en oeuvre d'environnements de développement. Il s'intéresse notamment à l'utilisation et à l'enrichissement de bibliothèques de composants logiciels et à la programmation, notamment au sein d'un framework.


## Les TP : 

* **TP1** : Initiation a Java
* **TP2** : Gestion des erreurs
* **TP3** : Objets et Classes / List
* **TP4** : Lecture / Écriture dans un fichier texte et XML
* **TP5** : Lecture / Ecriture dans une base de donnée
* **TP6** : Mise en place d'une version graphique du pentathlon moderne

## Projets: 
* **Penthatlon moderne** : Dans le cadre de notre formation, il nous a été demandé de concevoir un programme simulant les épreuves du penthatlon moderne. 
