package edf.btssio.org.edf;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Iterator;

public class TestClientActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        Button btnAjout = (Button) findViewById(R.id.btnGo);

        btnAjout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                testBD();
            }
        });


    }
    private void testBD()
    {
        ClientDAO viticulteurAcces = new ClientDAO(this);

        // recuperation des valeurs saisies
        EditText txtNomV = (EditText) findViewById(R.id.edtNom);
        EditText txtPrenom = (EditText) findViewById(R.id.edtPrenom);

        Client nouveauClient = new Client(txtNomV.getText().toString(), txtPrenom.getText().toString());

        viticulteurAcces.addClient(nouveauClient);


        // test :
        ArrayList<Client> listeClientRecherche;
        listeClientRecherche = viticulteurAcces.getClients("%");

        Iterator<Client> itClient;
        Client unViticulteur;
        for( itClient = listeClientRecherche.iterator(); itClient.hasNext(); ){
            unViticulteur = itClient.next();
            Log.d("log",unViticulteur.toString());
        }
    }
}
