package edf.btssio.org.edf;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageButton btnIdentifiant;
    ImageButton btnReleverCompteur;
    ImageButton btnSauvegarde;
    ImageButton btnImportDonnes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnIdentifiant = (ImageButton)findViewById(R.id.imgID);
        btnIdentifiant.setOnClickListener(this);

        btnReleverCompteur = (ImageButton)findViewById(R.id.imgRelever);
        btnReleverCompteur.setOnClickListener(this);

        btnSauvegarde = (ImageButton)findViewById(R.id.imgSave);
        btnSauvegarde.setOnClickListener(this);

        btnImportDonnes = (ImageButton)findViewById(R.id.imgImport);
        btnImportDonnes.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgID:
                Intent intent = new Intent(this, TestClientActivity.class);
                startActivity(intent);
                break;
        }
    }
}
