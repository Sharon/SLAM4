package edf.btssio.org.edf;

public class Client {
    // Données ne pouvant être modifiées cf service commercial/financier
    private int identifiant ;
    private String nom,prenom,adresse,codePostal,ville,telephone;
    private String idCompteur;
    private Double ancienReleve;
    private String dateAncienReleve;

    // Données à saisir
    private Double dernierReleve;
    private String dateDernierReleve; // oui pour simplifier la date sera une chaîne
    private String signatureBase64;
    private int situation;
    /* Exemples de situation client ------
     * 0 client non traité par défaut
     * 1 Absent
     * 2 Absent mais relevé possible sans signature client
     * 3 Présent, relevé ok mais pas de signature car pas représentant légal
     * 4 Présent et tout ok
     * 5 Déménagé / logement vide
     * 6 Déménagé / nouveaux locataires
     * 72,73,74 idem 2,3,4 mais dysfonctionnement
     * 82,83,84 idem 2,3,4 mais dysfonctionnement / dégradation
     * ... etc etc
     */

    public int getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getIdCompteur() {
        return idCompteur;
    }

    public void setIdCompteur(String idCompteur) {
        this.idCompteur = idCompteur;
    }

    public Double getAncienReleve() {
        return ancienReleve;
    }

    public void setAncienReleve(Double ancienReleve) {
        this.ancienReleve = ancienReleve;
    }

    public String getDateAncienReleve() {
        return dateAncienReleve;
    }

    public void setDateAncienReleve(String dateAncienReleve) {
        this.dateAncienReleve = dateAncienReleve;
    }

    public Double getDernierReleve() {
        return dernierReleve;
    }

    public void setDernierReleve(Double dernierReleve) {
        this.dernierReleve = dernierReleve;
    }

    public String getDateDernierReleve() {
        return dateDernierReleve;
    }

    public void setDateDernierReleve(String dateDernierReleve) {
        this.dateDernierReleve = dateDernierReleve;
    }

    public String getSignatureBase64() {
        return signatureBase64;
    }

    public void setSignatureBase64(String signatureBase64) {
        this.signatureBase64 = signatureBase64;
    }

    public int getSituation() {
        return situation;
    }

    public void setSituation(int situation) {
        this.situation = situation;
    }

    public Client(int identifiant, String nom, String prenom, String adresse, String codePostal, String ville, String telephone, String idCompteur, Double ancienReleve, String dateAncienReleve, Double dernierReleve, String dateDernierReleve, String signatureBase64, int situation) {
        this.identifiant = identifiant;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.idCompteur = idCompteur;
        this.ancienReleve = ancienReleve;
        this.dateAncienReleve = dateAncienReleve;
        this.dernierReleve = dernierReleve;
        this.dateDernierReleve = dateDernierReleve;
        this.signatureBase64 = signatureBase64;
        this.situation = situation;
    }

    public Client(int identifiant, String nom, String prenom, String adresse, String codePostal, String ville, String telephone, String idCompteur, Double ancienReleve, String dateAncienReleve) {
        this.identifiant = identifiant;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.idCompteur = idCompteur;
        this.ancienReleve = ancienReleve;
        this.dateAncienReleve = dateAncienReleve;
        this.dernierReleve = 0.0;
        this.dateDernierReleve = "02/04/2018";
        this.signatureBase64 = "";
        this.situation = 0;
    }

    public Client(int identifiant, String nom, String prenom) {
        this.identifiant = identifiant;
        this.nom = nom;
        this.prenom = prenom;
    }

    public Client(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }
}
