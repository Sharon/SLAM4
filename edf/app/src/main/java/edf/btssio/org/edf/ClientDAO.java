package edf.btssio.org.edf;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ClientDAO {
    private static String base = "BDViticulteur";
    private static int version = 1;
    private BdSQLiteOpenHelper accesBD;

    public ClientDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);

    }

    public void addClient(Client unClient){
        SQLiteDatabase bd = accesBD.getWritableDatabase();

        String req = "insert into client(nom,prenom)"
                + " values('"+unClient.getNom()+"',"+unClient.getPrenom()+");";
        Log.d("log",req);
        bd.execSQL(req);
        bd.close();
    }

    public Client getClient(int id){
        Client leClient = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from client where id="+id+";",null);
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            leClient = new Client(id,curseur.getString(1), curseur.getString(2));
        }
        return leClient;
    }
    public ArrayList<Client> getClients(String nom){
        Cursor curseur;
        String req = "select * from client where nom like '"+nom+"';";
        curseur = accesBD.getReadableDatabase().rawQuery(req,null);
        return cursorToClientArrayList(curseur);
    }
    private ArrayList<Client> cursorToClientArrayList(Cursor curseur){
        ArrayList<Client> listeClient = new ArrayList<Client>();
        int id;
        String nom;
        String prenom;

        curseur.moveToFirst();
        while (!curseur.isAfterLast()){
            id = curseur.getInt(0);
            nom = curseur.getString(1);
            prenom = curseur.getString(2);
            listeClient.add(new Client(id,nom,prenom));
            curseur.moveToNext();
        }

        return listeClient;
    }

}
