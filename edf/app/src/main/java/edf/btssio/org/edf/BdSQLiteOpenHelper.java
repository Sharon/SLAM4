package edf.btssio.org.edf;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

    public class BdSQLiteOpenHelper extends SQLiteOpenHelper {


        public BdSQLiteOpenHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub
            String requete = "create table client ("
                    + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + "nom TEXT NOT NULL,"
                    + "prenom TEXT NOT NULL);";
            db.execSQL(requete);

            db.execSQL("insert into client (nom,prenom) values('Garay',50);");

            ContentValues value = new ContentValues();
            value.put("nomV", "Esquerra");
            value.put("prenom", "TESTs");
            db.insert("client", null, value);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub

        }
    }
