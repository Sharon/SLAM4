package Version3_2;

import java.io.File;
import java.io.FileOutputStream;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import javax.xml.parsers.DocumentBuilder;

import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.Node;


import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.w3c.dom.Element;

import org.xml.sax.SAXException;

import Version2.Athlete;

/**
 * Classe principal du programme du projet PENTHATLON MODERNE
 * @author Panthier Sharon
 * @version 2
 * 
 */
public class principal {
	static Scanner sc = new Scanner(System.in);
	static List<Athlete> lesAthletes = new ArrayList<>();
	static int idAthlete = 0 ;
	static String filename = "src/Version3_2/penthatlon.xml";
	/**
	 * Permet l'ajout de nouveaux athletes
	 */
	public static void AjouterAthlete() {
		int age;
		boolean ok = false;
		String nationalite;
		while(!ok) 
		{
			pentathlon_moderne.Write("Athlete par défaut ? y/n: ", false);
			String defaultA = sc.next().toLowerCase();
			if(defaultA.equals("y")) {
				lesAthletes.add(new Athlete());
				ok =true;
			} else if(defaultA.equals("n")) {
				pentathlon_moderne.Write("Veuillez saisir le nom de l'athlete : ", false);
				String nomAthlete = sc.next();
			
				pentathlon_moderne.Write("Veuillez saisir le prenom de l'athlete : ", false);
				String prenomAthlete = sc.next();	
				
				age = VerifAgeAthlete();
				nationalite = VerifNationalite();
				
				lesAthletes.add(new Athlete(nomAthlete, prenomAthlete,nationalite,age));
				ok = true;
			} else {
				ok = false;
			}
		}
		System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
		pentathlon_moderne.Escrime(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
		
		// on vérifie d'abord si l'athlete n'a pas été disqualifié
		if(!pentathlon_moderne.disqualifier) {
			if(!pentathlon_moderne.Abandon()) { // Puis on vérifie si il n'a pas abandonner
				System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
				pentathlon_moderne.Natation(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
				if(!pentathlon_moderne.disqualifier) {
					if(!pentathlon_moderne.Abandon()) {
						System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
						pentathlon_moderne.Equitation(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
						if(!pentathlon_moderne.disqualifier) {
							if(!pentathlon_moderne.Abandon()) {
								System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
								pentathlon_moderne.Course(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
							}
						}
					}
				}
			}
		}
		if(pentathlon_moderne.disqualifier || pentathlon_moderne.abandon) {
			pentathlon_moderne.FinDuJeu();
		}
		Recommencer();			
	}
	private static String VerifNationalite() {
		boolean ok = false;
		String natio = "";
		while(!ok)
		{
			int i = 1;
			List<String> LesNationalite = new ArrayList<>();
			LesNationalite.add(0, "français");
			LesNationalite.add(1, "anglais");
			LesNationalite.add(2, "japonais");
			LesNationalite.add(3, "africain");
			LesNationalite.add(4, "chinois");
			LesNationalite.add(5, "allemand");
			LesNationalite.add(5, "russe");
			for(String str : LesNationalite) {
				System.out.println(i + " - " +str +" ");
				i++;
			}
			
			pentathlon_moderne.Write("Veuillez saisir la nationalité de l'athlete : ", false);
			
			if(sc.hasNextInt()) {
				int choix = sc.nextInt();
				ok = true;
				if(choix < (LesNationalite.size()+1)) {
					choix -= 1;
					natio = LesNationalite.get(choix);
				} else {
					ok = false;
				}

			} else {
				sc.next();
				ok = false;
			}
		}

		return natio;
	}
	private static int VerifAgeAthlete() {
		boolean ok = false;
		int age = 0;
		
		while(!ok) 
		{
			System.out.print("Veuillez saisir l'age de l'athlete : ");
				
			if(sc.hasNextInt()) {
				age = sc.nextInt();	
				if (age > 20 && age < 50) 
					ok = true;
				 else {
						System.out.println("l'athlete doit avoir entre 20 et 50 ans ");
						ok = false;
					}
				} else {
					sc.next();
					ok = false;
				}
		}
		return age;
	}
	/**
	 * Verifie si l'utilisateur veux recommencer le penthatlon
	 */
	public static void Recommencer()
	{
		boolean ok = false;
		while(!ok)
		{
			pentathlon_moderne.Write("Voulez vous ajouter un nouvel athlete ? O / N : ", false);
			String recommencer = sc.next().toLowerCase();

				if(recommencer.equals("o")) {
					ok = true;
					idAthlete++;
					pentathlon_moderne.disqualifier = false;
					pentathlon_moderne.abandon = false;
					AjouterAthlete();
				} else if (recommencer.equals("n")){
					ok = true;
					pentathlon_moderne.Write("Arrêt du programme.", true);
					pentathlon_moderne.FinDuJeu();
				} else {
					System.err.println("Erreur : Il faut choisir entre O et N.");
					ok = false;
				}
		}
	}
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException 
	{
		pentathlon_moderne.Write("\t\t\t*******PENTATHLON MODERNE*********", true);
		Lecture();
		AjouterAthlete();
	}
	public static void Lecture() throws ParserConfigurationException, SAXException, IOException
	{
		DocumentBuilderFactory unNomDObjet = DocumentBuilderFactory.newInstance();
		DocumentBuilder unDB = unNomDObjet.newDocumentBuilder();
		Document leDocument= unDB.parse(new File(filename));
			
		Element racine = leDocument.getDocumentElement();
		NodeList racineNoeuds = racine.getChildNodes();
		String nomA =" ";
		String prenomA="";
		String nationaliteA="";
		int ageA = 0;
		int nbpoint = 0;
			
		for (int i = 0; i<racineNoeuds.getLength(); i++) {
			if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
			Element unAthlete= (Element) racineNoeuds.item(i);

			Element nom = (Element) unAthlete.getElementsByTagName("nom").item(0);
			Element prenom = (Element) unAthlete.getElementsByTagName("prenom").item(0);
			Element nationalite = (Element) unAthlete.getElementsByTagName("nationalite").item(0);
			Element age = (Element) unAthlete.getElementsByTagName("age").item(0);
			Element nbpoints = (Element) unAthlete.getElementsByTagName("points").item(0);
				
			nomA = nom.getTextContent();
			prenomA = prenom.getTextContent();
			nationaliteA = nationalite.getTextContent();
			ageA = Integer.parseInt(age.getTextContent());
			nbpoint = Integer.parseInt(nbpoints.getTextContent());
			
			lesAthletes.add(new Athlete(nomA, prenomA, nationaliteA, ageA, nbpoint));
			idAthlete++;
			
			}
		}
	}
	public static void ecrire()
	{
		
		// création de la racine XML, ici "penthatlon"
		org.jdom2.Element racine = new org.jdom2.Element("penthatlon");
		//On crée un nouveau Document JDOM basé sur la racine que l'on vient de créer
		org.jdom2.Document leDoc = new org.jdom2.Document(racine);
		//On crée un nouvel Element athlete et on l'ajoute en tant qu'Element de racine

		//On crée un nouvel Element nom, on lui assigne du texte et on l'ajoute en tant
		// qu'Element de athlete
		for(Athlete ath : lesAthletes)
		{
			org.jdom2.Element athlete = new org.jdom2.Element("athlete");
			racine.addContent(athlete);
			
			org.jdom2.Element rang = new org.jdom2.Element("rang");
			rang.addContent(String.valueOf(ath.getRang()));
			athlete.addContent(rang);
			
			org.jdom2.Element nom = new org.jdom2.Element("nom");
			nom.addContent(ath.getNom());
			athlete.addContent(nom);

			org.jdom2.Element prenom = new org.jdom2.Element("prenom");
			prenom.addContent(ath.getPrenom());
			athlete.addContent(prenom);
			
			org.jdom2.Element age = new org.jdom2.Element("age");
			age.addContent(String.valueOf(ath.getAge()));
			athlete.addContent(age);
			
			org.jdom2.Element nationalite = new org.jdom2.Element("nationalite");
			nationalite.addContent(ath.getNationalite());
			athlete.addContent(nationalite);
			
			org.jdom2.Element points = new org.jdom2.Element("points");
			points.addContent(String.valueOf(ath.getNbPoints()));
			athlete.addContent(points);
		}
		try
		{
		//On utilise ici une sortie classique avec getPrettyFormat()
		XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
		// création d’une instance de FileOutputStream avec en argument le nom du fichier
		sortie.output((org.jdom2.Document) leDoc, new FileOutputStream(filename));
		} catch (java.io.IOException e){
			System.err.println("erreur - écriture fichier xml");
		}
	}

}
