package Version3_2;

import java.util.Collections;
import java.util.Scanner;

import Version2.Athlete;

/**
 * Classe regroupant toutes les epreuves du penthatlon moderne
 * @author PANTHIER Sharon
 * @version 2
 * 
 */
public class pentathlon_moderne {
	// Variables globales
	static Scanner sc = new Scanner(System.in);
	public static boolean disqualifier = false;
	public static boolean abandon = false;
	
	/**
	 * Demande et verifie si l'utiiisateur souhaite abandonner
	 * @return booleen pour savoir si on continue ou non
	 */

	public static boolean Abandon() {
		String decision;
		boolean ok = false;
		while(!ok)
		{
			System.out.print("Voulez-vous abandonner ? (O/N) : ");
			decision = sc.next();

			if (decision.equals("o") || decision.equals("O")) {
				System.out.println("Vous avez abandonner ! ");
				abandon = true;
				ok = true;
			} else if (decision.equals("n") || decision.equals("N")) {
				principal.lesAthletes.get(principal.idAthlete).ChangerEpreuve();
				ok = true;
			} else {
				System.err.println("La saisie doit obligatoirement être 'O' ou 'N'");
				ok=false;
			}
		}
		return abandon;
	}
	/**
	 * Affiche que l'utilisateur est disqualifie
	 * @return booleen pour savoir si l'utilisateur est disqualifie
	 */
	public static boolean Disqualifier() {
		principal.lesAthletes.get(principal.idAthlete).setNbPoints(0);
		Write("Vous êtes discalifié ! ", true);
		return disqualifier = true;
	}
	/**
	 * Determine la fin du penthatlon moderne + écriture des athlètes et des scores dans un fichier texte
	 */
	public static void FinDuJeu() {
		principal.ecrire();
	}

	// ********************* EPREUVES ************************

	// --------------- ESCRIME --------------------------------
	/**
	 * Epreuve d'escrime
	 * @param prenomAthlete nom de l'athlete 
	 * @param nomAthlete prenom de l'athlete
	 */
	public static void Escrime(String prenomAthlete, String nomAthlete) {
		boolean ok = false ; 
		//System.out.print(principal.idAthlete);
		while(!ok)
		{
			Write("Veuillez saisir le pourcentage d'assaut gagné : ", false);
			if (sc.hasNextInt()) {
				int assaut = sc.nextInt();
				ok = true;
				
				if (assaut > 0 && assaut <= 100) {
					if (assaut < 10) {
						Disqualifier();
					} else if (assaut < 25) {
						principal.lesAthletes.get(principal.idAthlete).AjouterScore(100);
						
					} else if (assaut < 50) {
						principal.lesAthletes.get(principal.idAthlete).AjouterScore(200);
					} else if (assaut < 65) {
						principal.lesAthletes.get(principal.idAthlete).AjouterScore(230);
					} else {
						principal.lesAthletes.get(principal.idAthlete).AjouterScore(250);
					}
					System.out.println(principal.lesAthletes.get(principal.idAthlete).AfficheScore());
				} else {
					System.out.println("Out of range");
					Escrime(prenomAthlete, nomAthlete);
				}
			} else {
				System.err.println("\nVeuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}

	}

	// ------------------ FIN EPREUVE ESCRIME -------------------------------------------

	// -------------------------- NATATION   --------------------------------------------
	/**
	 * Epreuve de natation
	 * @param prenomAthlete nom de l'athlete 
	 * @param nomAthlete prenom de l'athlete
	 */
	public static void Natation(String prenomAthlete, String nomAthlete) {
		boolean ok = false;
		while(!ok)
		{
			Write("Quel a été le temps de l'épreuve ? : ", false);
			int tempsEpreuve = 0;

			if (sc.hasNextInt()) {
				tempsEpreuve = sc.nextInt();
				ok = true;
				// Verification du temps, si celui-ci est supérieur a 210 on arrête
				// le programme direct
				if (tempsEpreuve <= 210) {
					Noyer(tempsEpreuve);
				} else {
					Disqualifier();	
				}
				System.out.println(principal.lesAthletes.get(principal.idAthlete).AfficheScore());

			} else {
				// Recall function Noyer
				System.err.print("\nVeuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}


	}
	/**
	 * Verifie si l'athlete s'est noye
	 * @param tempsEpreuve recupere le temps de l'epreuve pour pouvoir calculer le nombre de points
	 */
	private static void Noyer(int tempsEpreuve) {
		int tempsIdeal = 150;
		int differenceTemps;

		Write("L'athlete s'est-il noyé ? : (o/n) ", false);
		String noyer = sc.next();

		// Si l'athlete s'est noyé, il est disqualifié
		if (noyer.equals("o") || noyer.equals("O")) {
			Disqualifier();
		} else if (noyer.equals("n") || noyer.equals("N")) {
			if (tempsEpreuve > tempsIdeal) {
				differenceTemps = tempsEpreuve - tempsIdeal;
				principal.lesAthletes.get(principal.idAthlete).AjouterScore(250 - differenceTemps);
			} else if (tempsEpreuve == tempsIdeal) {
				principal.lesAthletes.get(principal.idAthlete).AjouterScore(250);
			} else {
				differenceTemps = tempsIdeal - tempsEpreuve;
				principal.lesAthletes.get(principal.idAthlete).AjouterScore(250 + differenceTemps);
			}
		}
	}

	// --------------- FIN EPREUVE NATATION --------------------------------

	// ------------------ EQUITATION ---------------------------------------
	/**
	 * Epreuve d'equitation
	 * @param prenomAthlete nom de l'athlete 
	 * @param nomAthlete prenom de l'athlete
	 */
	public static void Equitation(String prenomAthlete, String nomAthlete) {
		boolean ok = false;
		while(!ok)
		{
			Write("Combien de barre l'athlete a-t-il fait tomber ? : ", false);
			if (sc.hasNextInt()) {
				int nbBarreAthlete = sc.nextInt();
				ok =true;
				Verif(nbBarreAthlete, 12, 10);

				// On verifie si le cheval fait tomber des barres
				ChevalTomberBarre();

				// On verifie si le cheval refuse de sauter
				RefuserSauter();

				// On vérifie si l'athlete est tombé du cheval
				TomberCheval(nomAthlete, prenomAthlete);
				
				System.out.println(principal.lesAthletes.get(principal.idAthlete).AfficheScore());
			} else {
				System.err.println("Veuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}
	}
	/**
	 * Verifie si le cheval refuse de sauter la barre
	 */
	private static void RefuserSauter() {
		boolean ok = false;
		while(!ok)
		{
			Write("Combien de fois le cheval a-t-il refusé de sauter ? : ", false);
			if (sc.hasNextInt()) {
				ok = true;
				int refus = sc.nextInt();
				// on vérifie que le cheval ne refuse pas de sauter plus de 3 fois
				Verif(refus, 3, 20);

			} else {
				System.err.println("Veuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}
	}
	/**
	 * Recupere le nombre de barre que le cheval a fait tomber
	 */
	private static void ChevalTomberBarre() {
		boolean ok = false;
		while(!ok)
		{
			Write("Combien de barre le cheval a-t-il fait tomber ? : ", false);
			if (sc.hasNextInt()) {
				ok = true;
				int nbBarreCheval = sc.nextInt();
				Verif(nbBarreCheval, 12, 10);
			} else {
				System.err.print("\nVeuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}
	}
	/**
	 * Verifie si l'athlete est tomber du cheval
	 * @param nom nom de l'athlete
	 * @param prenom prenom de l'athlete
	 */
	private static void TomberCheval(String nom, String prenom) {
		boolean ok = false;
		while(!ok)
		{
			Write("L'athlète est-il tombé ? (o/n) : ", false);
			String tombeAthlete = sc.next();

			if (tombeAthlete.equals("o") || tombeAthlete.equals("O")) {
				Disqualifier();
				ok = true;
			} else if (tombeAthlete.equals("n") || tombeAthlete.equals("N")) {
				ok = true;
			} else {
				System.err.println("Vous devez choisir entre O ou N ! ");
				ok = false;
			}
		}
	}
	/**
	 * Verifie que la saisie de l'utilisateur ne depasse pas une certine limite puis ajoute les points a l'athlete
	 * @param nbSaisie saisie de l'utilisateur 
	 * @param limite limite definie par type d'epreuve
	 * @param nbPoints nombre de points a attribuer
	 */
	private static void Verif(int nbSaisie, int limite, int nbPoints) {
		// On vérifie que la saisie n'est pas en dehors des limites fixées par l'épreuve
		boolean ok = false;
		while(!ok)
		{
			if (nbSaisie <= limite) {
				principal.lesAthletes.get(principal.idAthlete).AjouterScore(100 - nbPoints * nbSaisie);
				ok = true;
			} else if (nbSaisie == 0) {
				principal.lesAthletes.get(principal.idAthlete).AjouterScore(100); 
				ok = true;
			} else {
				System.err.println("Le nombre que vous avez saisi est " + nbSaisie + " superieur à " + limite + " veuillez le re-saisir :");
				nbSaisie = sc.nextInt();
				ok = false;
			}
		}


	}

	// --------------- FIN EPREUVE EQUITATION --------------------------------

	// --------------- COURSE / TIR A L'ARC -----------------------------------
	/**
	 * Epreuve de course / tir a l'arc
	 * @param prenomAthlete nom de l'athlete 
	 * @param nomAthlete prenom de l'athlete
	 */
	public static void Course(String prenomAthlete, String nomAthlete) {
		Collections.sort(principal.lesAthletes);
		for (Athlete ath : principal.lesAthletes)
		{
			// Compte le nombre point au total de l'athlete
			ath.CompteScore();
			System.out.println("N°" +ath.getRang()+" - l'athlete " + ath.getNom() +" "+ath.getPrenom() + " "+ ath.getNbPoints() + " points.");

		}
	}
	// --------------- FIN EPREUVE COURSE / TIR A L'ARC --------------------------------

	// Procédure utilitaires
	/**
	 * Procedure qui sert a l'affichage et determine qi l'on veut retourner a la ligne ou non
	 * @param str phrase a afficher
	 * @param aLaLigne retour a la ligne ou non
	 */
	public static void Write(String str, boolean aLaLigne) {
		if (aLaLigne == true) {
			System.out.println(str);
		} else {
			System.out.print(str);
		}

	}
}
