package Version5;

import java.awt.Color;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class escrime extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static JTextField tbxpourcentage = new JTextField();
	
	public static JLabel presentationEscrime,lblpourcentage,lblAbandon; 
	public static JButton btnValider = new JButton("Valider");
	public static JRadioButton radioButtonAbandonYes = new JRadioButton("oui");
	public static JRadioButton radioButtonAbandonNo = new JRadioButton("non");
	
	
	public escrime()
	{	
		// Labels
		presentationEscrime = new JLabel("Epreuve d'escrime");
        presentationEscrime.setLocation(237, 26);
        presentationEscrime.setSize(150, 18);
		add(presentationEscrime);
		
		lblpourcentage = new JLabel("Pourcentage de touché : ");
		lblpourcentage.setLocation(20,70);
		lblpourcentage.setSize(200, 18);
		add(lblpourcentage);
		
		lblAbandon = new JLabel("Voulez vous abandonner ?");
		lblAbandon.setLocation(20,100);
		lblAbandon.setSize(200,20);
		add(lblAbandon);
		
		// TextField
        tbxpourcentage.setLocation(200, 72);
        tbxpourcentage.setSize(25,15);
		add(tbxpourcentage);
		
        
        ButtonGroup bg =  new ButtonGroup();
        
        // RadioButton
        radioButtonAbandonNo.setBounds(240,100,60,20);
        radioButtonAbandonNo.setBackground(Color.CYAN);
        radioButtonAbandonNo.setSelected(true);
        
        radioButtonAbandonYes.setBounds(300,100,60,20);
        radioButtonAbandonYes.setBackground(Color.CYAN);

        bg.add(radioButtonAbandonNo);
        bg.add(radioButtonAbandonYes);
        
        add(radioButtonAbandonYes);
        add(radioButtonAbandonNo);

		// Button
        btnValider.setBounds(450,150, 120, 30);
        add(btnValider);
        
        setLayout(null);
        setBounds(0, 130, 600, 350);
        setBackground(Color.cyan);
        setVisible(false);
	}

}
