package Version5;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Version2.Athlete;

public class principalv5 extends JFrame implements ActionListener{
	private static JButton btnValider = new JButton("Valider");
	private static JLabel lblNom, lblPrenom, lblNationalite, lblAge;
	private static JComboBox<String> cbxnationalite = new JComboBox<>();
	private static escrime panelescrime = new escrime();
	private static natation panelnatation = new natation();
	private static endGame panelFin = new endGame();
	private static equitation panelequitation = new equitation();
	private static courseTir panelcourse = new courseTir();
	static JLabel presentation = new JLabel("Pentathlon Moderne");
	public static Athlete ath;
	public static JTextField tbxNom, tbxPrenom, tbxAge;
	static JPanel  pan=new JPanel();
	
	public principalv5(){
		super("Pentathlon moderne");
		
		
		build();

	}
	public void build() {
        //panel
       
       
        presentation.setBounds(200, 1, 200, 50);
        pan.add(presentation);
        
        
        // JLabel - Inscritpion de l'athlete
        lblNom = new JLabel("Nom : ");
        lblNom.setBounds(10, 20, 50, 50);
        pan.add(lblNom);
        
        lblPrenom = new JLabel("Prenom : ");
        lblPrenom.setBounds(10, 40, 70, 50);
        pan.add(lblPrenom);
        
        lblNationalite = new JLabel("Nationalité : ");
        lblNationalite.setBounds(10, 60, 100, 50);
        pan.add(lblNationalite);
        
        lblAge = new JLabel("Age : ");
        lblAge.setBounds(10, 100, 100, 20);
        pan.add(lblAge);
     
        // TextField - Saisie de l'utisateur
        tbxNom=new JTextField();
        tbxNom.setBounds(105, 40, 100,20);
        pan.add(tbxNom);
        
        tbxPrenom=new JTextField();
        tbxPrenom.setBounds(105, 60, 100,20);
        pan.add(tbxPrenom);
         
        tbxAge = new JTextField();
        tbxAge.setColumns(2);
        tbxAge.setBounds(105, 100, 100, 20);
 
        pan.add(tbxAge);
        
        // ComboBox pour les différentes nationalités
        cbxnationalite.setBounds(105, 80, 100, 20);
        cbxnationalite.setToolTipText("Cliquer pour choisir la nationalité de l'athlète");
        cbxnationalite.addItem("Français");
        cbxnationalite.addItem("Anglais");
        cbxnationalite.addItem("Allemand");
        cbxnationalite.addItem("Russe");
        cbxnationalite.addItem("Chinois");
        pan.add(cbxnationalite);
        
        
        //JButton -  Valider
        btnValider.addActionListener(this);
        btnValider.setBounds(448,100, 100, 20);
        pan.add(btnValider);

        
        // ESCRIME
        Version5.escrime.btnValider.addActionListener(this);
        Version5.escrime.radioButtonAbandonYes.addActionListener(this);
        Version5.escrime.radioButtonAbandonNo.addActionListener(this);
        
        // NATATION
        natation.btnValider.addActionListener(this);
        natation.radioButtonAbandonNo.addActionListener(this);
        natation.radioButtonAbandonYes.addActionListener(this);
        natation.radioButtonNoyerYes.addActionListener(this);
        natation.radioButtonNoyerNo.addActionListener(this);
        
        // ESQUITATION
        equitation.btnValider.addActionListener(this);
        
        // COURSE / TIR
        courseTir.btnValider.addActionListener(this);
        
        pan.setVisible(true);
        
        pan.add(panelescrime);
        pan.add(panelFin);
        pan.add(panelnatation);
        pan.add(panelequitation);
        pan.add(panelcourse);
        
        setContentPane(pan);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);
        setSize(600, 350);
        setVisible(true);
		
	}
	public boolean CheckEmptyField(JTextField tbx)
	{
		String field=tbx.getText();
		if(!field.isEmpty()){
			System.out.println(field + " - pas vide");
			return false;
		}else{
			javax.swing.JOptionPane.showMessageDialog(null,"Le champ ne peut pas être vide");	
			return true;
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// Inscription de l'athlète
		if(e.getSource()==btnValider) {
			try {
				
				if(!CheckEmptyField(tbxNom) && !CheckEmptyField(tbxPrenom) && !CheckEmptyField(tbxAge)) {
					
					int Age = Integer.parseInt(tbxAge.getText());
					// Vérification de la saisie de l'utilisateur
					if(Age < 0 || Age > 70) {
						tbxAge.setText("");
						Age = Integer.parseInt(tbxAge.getText());
					}
					
					ath = new Athlete(tbxNom.getText(), tbxPrenom.getText(), cbxnationalite.getSelectedItem().toString(),Age);
					btnValider.setEnabled(false);
					tbxNom.setEditable(false);
					tbxPrenom.setEditable(false);
					tbxAge.setEditable(false);
					cbxnationalite.setEditable(false);
					panelescrime.setVisible(true);
				}
			} catch (Exception e1) {
				// TODO: handle exception
				System.err.print(e1.getMessage());
				javax.swing.JOptionPane.showMessageDialog(null,"L'age saisie doit etre compris en 0 et 70 ! ");
			} 
		// Epreuve d'escrime
		} else if(e.getSource()==Version5.escrime.btnValider) {
			try {
				if(!CheckEmptyField(Version5.escrime.tbxpourcentage))
				{				
					int Assaut = Integer.parseInt(Version5.escrime.tbxpourcentage.getText());
					if(Assaut>0 && Assaut<= 100) {
						pentathlonv5.Escrime(Assaut);
						if(!pentathlonv5.disqualifier) {
							if(Version5.escrime.radioButtonAbandonNo.isSelected())  {
								panelnatation.setVisible(true);
								panelescrime.setVisible(false);
								presentation.setForeground(Color.red);
								presentation.setSize(250,15);
								panelescrime.tbxpourcentage.setText("");
								presentation.setText(ath.AfficheScore());
							}
							else {
								panelescrime.setVisible(false);
								panelFin.setVisible(true);
								panelFin.lblpresentation.setText(ath.AfficheScore());
							}
						} else {
							panelescrime.setVisible(false);
							panelFin.setVisible(true);
							panelFin.lblpresentation.setText(ath.AfficheScore());
						}
					} else {
						panelescrime.tbxpourcentage.setText("");
						Assaut = Integer.parseInt(Version5.escrime.tbxpourcentage.getText());
					}
				}
			} catch (Exception e2) {
				System.err.print(e2.getMessage());
				javax.swing.JOptionPane.showMessageDialog(null,"pourcentage doit etre compris entre 0 et 100");
			}
		// Epreuve de natation
		} else if(e.getSource()==natation.btnValider) {
			try {
				String noyer = "";
				int TempsEpreuve = 0;
				if(natation.radioButtonNoyerYes.isSelected()) {
					noyer = "o";
				} else {
					noyer = "n";
				}
				if(!CheckEmptyField(panelnatation.tbxTempsEpreuve)) {
					TempsEpreuve= Integer.parseInt(natation.tbxTempsEpreuve.getText());
					if(TempsEpreuve > 0) {
						pentathlonv5.Natation(TempsEpreuve, noyer);
						System.out.print(pentathlonv5.disqualifier);
						if(!pentathlonv5.disqualifier) {
							if(natation.radioButtonAbandonNo.isSelected() && natation.radioButtonNoyerNo.isSelected()) {
								panelequitation.setVisible(true);
								presentation.setText(ath.AfficheScore());
								panelnatation.setVisible(false);
							} else {
								panelFin.setVisible(true);
								panelFin.lblpresentation.setText(ath.AfficheScore());
								panelnatation.setVisible(false);
							}
						} else {
							panelFin.setVisible(true);
							panelFin.lblpresentation.setText(ath.AfficheScore());
							panelnatation.setVisible(false);
							panelFin.lblpresentation.setText(ath.AfficheScore());
						}	
					} else {
						panelnatation.tbxTempsEpreuve.setText("");
						TempsEpreuve= Integer.parseInt(natation.tbxTempsEpreuve.getText());
					}
				}
			} catch (Exception e2) {
				System.err.print(e2.getMessage());
				javax.swing.JOptionPane.showMessageDialog(null,"Le temps doit commencer a 100");
			}
		// Epreuve d'equitation
		} else if(e.getSource()==equitation.btnValider) {
			String cdtError = "";
			try
			{
				if(!CheckEmptyField(equitation.tbxTomberBarreAthlete) && !CheckEmptyField(equitation.tbxTomberBarreCheval) && !CheckEmptyField(equitation.tbxRefusSauter)) {
					int NbBarreAthlete, NbBarreCheval, Refus;
					NbBarreAthlete = Integer.parseInt(equitation.tbxTomberBarreAthlete.getText());
					NbBarreCheval = Integer.parseInt(equitation.tbxTomberBarreCheval.getText());
					Refus = Integer.parseInt(equitation.tbxRefusSauter.getText());

					if(NbBarreAthlete <= 12 &&  NbBarreCheval <= 12 &&  Refus <= 3 ) {
						pentathlonv5.Equitation(NbBarreAthlete, Refus, NbBarreCheval);
						if(!pentathlonv5.disqualifier) {
							if(equitation.radioButtonAbandonNo.isSelected() && equitation.radioButtonTomberNo.isSelected()) {
								panelcourse.setVisible(true);
								presentation.setText(ath.AfficheScore());
								courseTir.presentation.setText(ath.toString());
								panelequitation.setVisible(false);
							} else {
								panelFin.setVisible(true);
								panelFin.lblpresentation.setText(ath.AfficheScore());
								panelequitation.setVisible(false);
							}
						} else {
							panelFin.setVisible(true);
							panelequitation.setVisible(false);
							panelFin.lblpresentation.setText(ath.AfficheScore());
						}
					} else {
						if(NbBarreAthlete < 0 || NbBarreAthlete > 12 ) {
							equitation.tbxTomberBarreAthlete.setText("");
							NbBarreAthlete = Integer.parseInt(equitation.tbxTomberBarreAthlete.getText());
							cdtError = "Le nombre de barre tombée par l'athlete doit être compris entre 0 et 12";
						} else if(NbBarreCheval < 0 || NbBarreCheval > 12 ) {
							equitation.tbxTomberBarreCheval.setText("");
							NbBarreCheval = Integer.parseInt(equitation.tbxTomberBarreCheval.getText());
							cdtError = "Le nombre de barre tombée par le cheval doit être compris entre 0 et 12";
						}else if(Refus < 0 || Refus > 3 ) {
							equitation.tbxRefusSauter.setText("");
							Refus = Integer.parseInt(equitation.tbxRefusSauter.getText());
							cdtError = "Le nombre de refus doit être compris entre 0 et 3";
						}else {
							cdtError = "Veuillez saisir un entier"; 
						}
					}
				}
			} catch (Exception e2) {
				javax.swing.JOptionPane.showMessageDialog(null,"Veuillez saisir une valeur correcte : " + cdtError);
				equitation.tbxTomberBarreAthlete.setText("");
				equitation.tbxTomberBarreCheval.setText("");
				equitation.tbxRefusSauter.setText("");
			}
		} else if (e.getSource() == courseTir.btnValider) {
			panelcourse.setVisible(false);
			panelFin.setVisible(true);
			panelFin.lblpresentation.setText("L'athlete " + ath.AfficheScore());
			courseTir.btnValider.setVisible(false);
		}
	} 
}
