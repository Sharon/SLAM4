package Version5;

import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class equitation extends JPanel{
	public static JLabel presentation,lblTomberBarreAthlete, lblTomberBarreCheval,lblAbandon, lblTomber, lblRefusSauter; 
	public static JTextField tbxTomberBarreAthlete = new JTextField();
	public static JTextField tbxTomberBarreCheval = new JTextField();
	public static JTextField tbxRefusSauter = new JTextField();
	public static JButton btnValider = new JButton("Valider");
	public static JRadioButton radioButtonAbandonYes = new JRadioButton("oui");
	public static JRadioButton radioButtonAbandonNo = new JRadioButton("non");
	public static JRadioButton radioButtonTomberYes = new JRadioButton("oui");
	public static JRadioButton radioButtonTomberNo = new JRadioButton("non");
	
	public equitation()
	{
		// Labels
					presentation = new JLabel("Epreuve d'equitation");
					presentation.setLocation(230, 20);
					presentation.setSize(150, 18);
					add(presentation);
					
					lblTomberBarreAthlete = new JLabel("Combien de barre l'athlete a-t-il fait tomber ? :");
					lblTomberBarreAthlete.setLocation(20,70);
					lblTomberBarreAthlete.setSize(360, 18);
					add(lblTomberBarreAthlete);
					
					lblTomberBarreCheval = new JLabel("Combien de barre le cheval a-t-il fait tomber ? :");
					lblTomberBarreCheval.setLocation(20,50);
					lblTomberBarreCheval.setSize(360, 18);
					add(lblTomberBarreCheval);
					
					lblRefusSauter = new JLabel("Combien de fois le cheval a-t-il refuser de sauter ? :");
					lblRefusSauter.setLocation(20,90);
					lblRefusSauter.setSize(390, 18);
					add(lblRefusSauter);
					
					lblAbandon = new JLabel("Voulez vous abandonner ?");
					lblAbandon.setLocation(20,150);
					lblAbandon.setSize(230,20);
					add(lblAbandon);
					
					lblTomber = new JLabel("Est-ce qu'il est tombé?");
					lblTomber.setLocation(20,120);
					lblTomber.setSize(230,20);
					add(lblTomber);
				
					
					// TextField
					tbxTomberBarreAthlete.setLocation(370, 72);
					tbxTomberBarreAthlete.setSize(35,15);
					add(tbxTomberBarreAthlete);
					
					tbxTomberBarreCheval.setLocation(370, 52);
					tbxTomberBarreCheval.setSize(35,15);
					add(tbxTomberBarreCheval);
					
					tbxRefusSauter.setLocation(400, 92);
					tbxRefusSauter.setSize(35,15);
					add(tbxRefusSauter);
					
					// ButtonGroup 
					ButtonGroup bgAbandon =  new ButtonGroup();
					ButtonGroup bgNoyade =  new ButtonGroup();
				        
				    // RadioButton
					radioButtonAbandonYes.setSize(60,20);
					radioButtonAbandonYes.setLocation(240,150);
					radioButtonAbandonYes.setBackground(Color.yellow);
				     
					radioButtonAbandonNo.setSize(60,20);
					radioButtonAbandonNo.setLocation(300,150);
					radioButtonAbandonNo.setBackground(Color.yellow);
				    radioButtonAbandonNo.setSelected(true);
				    
					
				    bgAbandon.add(radioButtonAbandonYes);
				    bgAbandon.add(radioButtonAbandonNo);
				        
				    add(radioButtonAbandonYes);
				    add(radioButtonAbandonNo);
					
				    radioButtonTomberYes.setSize(60,20);
				    radioButtonTomberYes.setLocation(240,120);
				    radioButtonTomberYes.setBackground(Color.yellow);
				     
				    radioButtonTomberNo.setSize(60,20);
				    radioButtonTomberNo.setLocation(300,120);
				    radioButtonTomberNo.setBackground(Color.yellow);
				    radioButtonTomberNo.setSelected(true);
				    
					bgNoyade.add(radioButtonTomberYes);
					bgNoyade.add(radioButtonTomberNo);
				        
				    add(radioButtonTomberYes);
				    add(radioButtonTomberNo);
				    
					// Button
			        btnValider.setBounds(450,150, 120, 30);
			        add(btnValider);
			        
				    setLayout(null);
				    setBounds(0, 130, 600, 350);
				    setBackground(Color.YELLOW);
				    setVisible(false);
	}
}
