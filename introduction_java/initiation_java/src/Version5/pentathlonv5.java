package Version5;

import java.util.Scanner;

public class pentathlonv5 {

	// Variables globales
	static Scanner sc = new Scanner(System.in);
	static boolean disqualifier = false;
	static boolean abandon = false;
	
	/**
	 * Affiche que l'utilisateur est disqualifie
	 * @return booleen pour savoir si l'utilisateur est disqualifie
	 */
	public static boolean Disqualifier() {

		System.out.println("Vous êtes discalifié ! ");

		return disqualifier = true;
	}

	
	// ********************* EPREUVES ************************

	// --------------- ESCRIME --------------------------------
	/**
	 * Epreuve d'escrime
	 * @param prenomAthlete nom de l'athlete 
	 * @param nomAthlete prenom de l'athlete
	 */
	public static void Escrime(int assaut) {	
		if (assaut > 0 && assaut<=100) {
			if (assaut < 10) {
				Disqualifier();
			} else if (assaut < 25) {
				principalv5.ath.AjouterScore(100);
			} else if (assaut < 50) {
				principalv5.ath.AjouterScore(200);
			} else if (assaut < 65) {
				principalv5.ath.AjouterScore(230);
			} else {
				principalv5.ath.AjouterScore(250);
			} 
		} 
	}
	// ------------------ FIN EPREUVE ESCRIME -------------------------------------------

	// -------------------------- NATATION   --------------------------------------------
	/**
	 * Epreuve de natation
	 * @param prenomAthlete nom de l'athlete 
	 * @param nomAthlete prenom de l'athlete
	 */
	public static void Natation(int tempsEpreuve, String noyer) {
		
			if (tempsEpreuve > 0 && tempsEpreuve <= 210) {
				Noyer(tempsEpreuve, noyer);
			} else {
				Disqualifier();	
			}
	}
	/**
	 * Verifie si l'athlete s'est noye
	 * @param tempsEpreuve recupere le temps de l'epreuve pour pouvoir calculer le nombre de points
	 */
	private static void Noyer(int tempsEpreuve, String noyer) {
		int tempsIdeal = 150;
		int differenceTemps;

		// Si l'athlete s'est noyé, il est disqualifié
		if (noyer.equals("o") || noyer.equals("O")) {
			Disqualifier();
		} else if (noyer.equals("n") || noyer.equals("N")) {
			if (tempsEpreuve > tempsIdeal) {
				differenceTemps = tempsEpreuve - tempsIdeal;
				principalv5.ath.AjouterScore(250 - differenceTemps);
			} else if (tempsEpreuve == tempsIdeal) {
				principalv5.ath.AjouterScore(250);
			} else {
				differenceTemps = tempsIdeal - tempsEpreuve;
				principalv5.ath.AjouterScore(250 + differenceTemps);
			}
		}
	}

	// --------------- FIN EPREUVE NATATION --------------------------------

	// ------------------ EQUITATION ---------------------------------------
	/**
	 * Epreuve d'equitation
	 * @param prenomAthlete nom de l'athlete 
	 * @param nomAthlete prenom de l'athlete
	 */
	public static void Equitation(int nbBarreAthlete, int refus, int nbBarreCheval) {

		Verif(nbBarreAthlete, 12, 10);

		// On verifie si le cheval fait tomber des barres
		ChevalTomberBarre(nbBarreCheval);

		// On verifie si le cheval refuse de sauter
		RefuserSauter(refus);

	}
	/**
	 * Verifie si le cheval refuse de sauter la barre
	 */
	private static void RefuserSauter(int refus) {
		Verif(refus, 3, 20);
	}
	/**
	 * Recupere le nombre de barre que le cheval a fait tomber
	 */
	private static void ChevalTomberBarre(int nbBarreCheval) {
		Verif(nbBarreCheval, 12, 10);
	}
	/**
	 * Verifie que la saisie de l'utilisateur ne depasse pas une certine limite puis ajoute les points a l'athlete
	 * @param nbSaisie saisie de l'utilisateur 
	 * @param limite limite definie par type d'epreuve
	 * @param nbPoints nombre de points a attribuer
	 */
	private static void Verif(int nbSaisie, int limite, int nbPoints) {
		// On vérifie que la saisie n'est pas en dehors des limites fixées par
		// l'épreuve
		if (nbSaisie <= limite) {
			principalv5.ath.AjouterScore(100 - nbPoints * nbSaisie); 
		} else if (nbSaisie == 0) {
			principalv5.ath.AjouterScore(100); 
		} else {
			System.out.print("Le nombre que vous avez saisi est " + nbSaisie + " superieur à " + limite + " veuillez le re-saisir :");
		}

	}
	// --------------- FIN EPREUVE EQUITATION --------------------------------

}
