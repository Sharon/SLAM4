package Version5;

import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class natation extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static JLabel presentationNatation,lblTempsEpreuve,lblAbandon, lblNoyade; 
	public static JTextField tbxTempsEpreuve = new JTextField();
	public static JButton btnValider = new JButton("Valider");
	public static JRadioButton radioButtonAbandonYes = new JRadioButton("oui");
	public static JRadioButton radioButtonAbandonNo = new JRadioButton("non");
	public static JRadioButton radioButtonNoyerYes = new JRadioButton("oui");
	public static JRadioButton radioButtonNoyerNo = new JRadioButton("non");
	
	public natation()
	{
			// Labels
			presentationNatation = new JLabel("Epreuve de natation");
			presentationNatation.setLocation(230, 20);
			presentationNatation.setSize(150, 18);
			add(presentationNatation);
			
			lblTempsEpreuve = new JLabel("Temps de l'épreuve :");
			lblTempsEpreuve.setLocation(20,70);
			lblTempsEpreuve.setSize(150, 18);
			add(lblTempsEpreuve);
			
			lblAbandon = new JLabel("Voulez vous abandonner ?");
			lblAbandon.setLocation(20,150);
			lblAbandon.setSize(230,20);
			add(lblAbandon);
			
			lblNoyade = new JLabel("Est-ce qu'il s'est noyer ?");
			lblNoyade.setLocation(20,100);
			lblNoyade.setSize(230,20);
			add(lblNoyade);
			
			// TextField
			tbxTempsEpreuve.setLocation(180, 72);
			tbxTempsEpreuve.setSize(35,15);
			add(tbxTempsEpreuve);
			
			ButtonGroup bgAbandon =  new ButtonGroup();
			ButtonGroup bgNoyade =  new ButtonGroup();
		        
		    // RadioButton
			radioButtonAbandonYes.setBounds(240,150,60,20);
			radioButtonAbandonYes.setBackground(Color.GREEN);
		     
			radioButtonAbandonNo.setBounds(300,150,60,20);
			radioButtonAbandonNo.setBackground(Color.green);
			radioButtonAbandonNo.setSelected(true);
			
		    bgAbandon.add(radioButtonAbandonYes);
		    bgAbandon.add(radioButtonAbandonNo);
		        
		    add(radioButtonAbandonYes);
		    add(radioButtonAbandonNo);
			
		    radioButtonNoyerYes.setBounds(240,100,60,20);
		    radioButtonNoyerYes.setBackground(Color.GREEN);

		    radioButtonNoyerNo.setBounds(300,100,60,20);
		    radioButtonNoyerNo.setBackground(Color.green);
		    radioButtonNoyerNo.setSelected(true);
		    
			bgNoyade.add(radioButtonNoyerYes);
			bgNoyade.add(radioButtonNoyerNo);
		        
		    add(radioButtonNoyerYes);
		    add(radioButtonNoyerNo);
		    
			// Button
	        btnValider.setBounds(450,150, 120, 30);
	        add(btnValider);
	        
		    setLayout(null);
		    setBounds(0, 130, 600, 350);
		    setBackground(Color.GREEN);
		    setVisible(false);
			
	}
}
