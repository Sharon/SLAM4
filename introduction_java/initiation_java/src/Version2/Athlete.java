package Version2;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Athlete
 * @author Panthier Sharon
 * @version 2
 * 
 */
public class Athlete extends Epreuve implements Comparable<Athlete> {
	private String nom;
	private String prenom;
	private List<Epreuve> score = new ArrayList<Epreuve>();
	private Integer nbPoints = 0;
	private String nationalite;
	private int age;
	private int rang;
	private int numEpreuve = 0;
	private static int cpt = 0;
	
	/**
	 * Constructeur par defaut
	 */
	public Athlete() {
		cpt++;
		setNom("Patrick");
		setPrenom("Bruel");
		age = 26;
		nationalite = "français";
		rang = cpt;
		score.add(new Epreuve("Escrime"));
		score.add(new Epreuve("Natation"));
		score.add(new Epreuve("Equitation"));
		score.add(new Epreuve("Course / Tir"));
	}
	/**
	 * Constructeur a 2 parametres
	 * @param nom nom de l'athlete
	 * @param prenom  prenom de l'athlete
	 */
	public Athlete(String nom, String prenom) {
		cpt++;
		this.rang = cpt;
		this.nom = nom;
		this.prenom = prenom;
		age = 26;
		nationalite = "français";
		score.add(new Epreuve("Escrime"));
		score.add(new Epreuve("Natation"));
		score.add(new Epreuve("Equitation"));
		score.add(new Epreuve("Course/Tir"));
	}
	/**
	 * Constructeur a 4 parametres
	 * @param nom nom de l'athlete
	 * @param prenom  prenom de l'athlete
	 * @param age age de l'athlete
	 * @param nationalite nationalite de l'athlete
	 */
	public Athlete(String nom, String prenom, String nationalite,int age, int nbPoints) {
		cpt++;
		this.rang = cpt;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.nbPoints = nbPoints;
		this.nationalite = nationalite;
		score.add(new Epreuve("Escrime"));
		score.add(new Epreuve("Natation"));
		score.add(new Epreuve("Equitation"));
		score.add(new Epreuve("Course/Tir"));
	}
	public Athlete(String nom, String prenom, String nationalite,int age) {
		cpt++;
		this.rang = cpt;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.nationalite = nationalite;
		score.add(new Epreuve("Escrime"));
		score.add(new Epreuve("Natation"));
		score.add(new Epreuve("Equitation"));
		score.add(new Epreuve("Course/Tir"));
	}
	/**
	 * Accesseur en lecture du nom de l'athlete
	 * @return nom de l'athlete
	 */
	public String getNom() {
		return nom;
	}
	/**
	 *  Accesseur en ecriture nom de l'athlete
	 * @param nom string nom de l'athlete
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * Accesseur en lecture du prenom de l'athlete
	 * @return string prenom de l'athlete
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * Accesseur en ecriture prenom de l'athlete
	 * @param prenom string prénom de l'athlete
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * Accesseur en lecture sur la liste d'epreuve
	 * @return score la liste des érepreuves
	 */
	public List<Epreuve> getScore() {
		return score;
	}
	/**
	 * Accesseur en lecture de la liste d'epreuve
	 * @param score int liste de scores
	 */
	public void setScore(List<Epreuve> score) {
		this.score = score;
	}
	/**
	 * Accesseur en lecture du nombre de points
	 * @return int nombre de points
	 */
	public Integer getNbPoints() {
		return nbPoints;
	}
	/**
	 * Acceseur en écriture du nombre de points
	 * @param nbPoints retourne le nombre d epoints de l'athlete
	 */
	public void setNbPoints(Integer nbPoints) {
		this.nbPoints = nbPoints;
	}
	/**
	 * Accesseur en lecture du rang de l'athlete
	 * @return le rang de l'athlete
	 */
	public int getRang() {
		return rang;
	}
	/**
	 * Accesseur en ecriture sur le rang de l'athlete
	 * @param rang int rang de l'athlete
	 */
	public void setRang(int rang) {
		this.rang = rang;
	}
	/**
	 * Accesseur en lecture de la nationalité de l'athlete
	 * @return nationalite de l'athlete
	 */
	public String getNationalite() {
		return nationalite;
	}
	/**
	 * Accesseur en ecriture de la nationalité de l'athlete
	 * @param nationalite nationalite de l'athlete
	 */
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
	/**
	 * Accesseur en lecture de l'age de l'athlete
	 * @return age de l'athlete
	 */
	public int getAge() {
		return age;
	}
	/**
	 * Accesseur en écriture de l'age de l'athlete
	 * @param age age de l'athlete
	 */
	public void setAge(int age) {
		this.age = age;
	}
	public int getNumEpreuve() {
		return numEpreuve;
	}
	/**
	 * Ajoute un score a la liste d'epreuve
	 * @param points int nombre de point a ajouter
	 */
	public void AjouterScore(int points) {
		score.get(numEpreuve).setPoints(points);
		nbPoints += points;
	}
	/**
	 * Affiche les epreuves et les points pbtenus
	 */
	public String toString() {
		String str = " ";
		str = "numéro " + rang +" - "+nom + " " + prenom + " a " + age + " ans et est " + nationalite + ".";
		return str;
	}
	/**
	 * Affiche le nom de l'epreuve dans laquelle in se situe
	 * @return str nom de l'epreuve
	 */
	public String AfficherEpreuve() {
		String str = " ";
		str = score.get(numEpreuve).toString();
		return str; 			
	}
	/**
	 * Compte le score au total
	 */
	public void CompteScore() {
		for(Epreuve ep : score)
		{
			nbPoints += ep.getPoints();
		}
	}
	/**
	 * Change le numero d'epreuves
	 */
	public void ChangerEpreuve() {
		numEpreuve++;
		//System.out.println("Epreuve N°"+numEpreuve);
		score.get(numEpreuve).setNum(numEpreuve+1);
	}
	/**
	 * Affiche les scores pour chaque epreuves
	 * @return str 
	 */
	public String AfficheScore() {
		return nom + " " + prenom + " a " + nbPoints + " points.";
	}
	@Override
	public int compareTo(Athlete ath) {
		return this.nbPoints.compareTo(ath.nbPoints);
	}
}
