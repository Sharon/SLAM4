package Version2;

import java.util.Collections;
import java.util.Scanner;

/**
 * Classe regroupant toutes les epreuves du penthatlon moderne
 * @author PANTHIER Sharon
 * @version 2
 * 
 */
public class pentathlon_moderne {
	// Variables globales
	static Scanner sc = new Scanner(System.in);
	static boolean disqualifier = false;
	static boolean abandon = false;
	

	
	/**
	 * Demande et verifie si l'utiiisateur souhaite abandonner
	 * @return booleen pour savoir si on continue ou non
	 */

	public static boolean Abandon() {
		String decision;

		System.out.print("Voulez-vous abandonner ? (O/N) : ");
		decision = sc.next();

		if (decision.equals("o") || decision.equals("O")) {
			System.out.println("Vous avez abandonner ! ");
			abandon = true;
		} else if (decision.equals("n") || decision.equals("N")) {
			principal.lesAthletes.get(principal.idAthlete).ChangerEpreuve();
		} else {
			System.out.println("La saisie doit obligatoirement être 'O' ou 'N'");
			Abandon();

		}
		return abandon;
	}
	/**
	 * Affiche que l'utilisateur est disqualifie
	 * @return booleen pour savoir si l'utilisateur est disqualifie
	 */
	public static boolean Disqualifier() {

		Write("Vous êtes discalifié ! ", true);

		return disqualifier = true;
	}
	/**
	 * Determine la fin du penthatlon moderne
	 */
	public static void FinDuJeu() {

			for (Athlete ath : principal.lesAthletes) 
			{
				ath.CompteScore();
				System.out.println("Fin des epreuves pour "+ath.getNom() + " " +ath.getPrenom() + " il a obtenu :  " + ath.getNbPoints() + " points.");
			}


	}
	
	// ********************* EPREUVES ************************

	// --------------- ESCRIME --------------------------------
	/**
	 * Epreuve d'escrime
	 * @param prenomAthlete nom de l'athlete 
	 * @param nomAthlete prenom de l'athlete
	 */
	public static void Escrime(String prenomAthlete, String nomAthlete) {
		Write("Veuillez saisir le pourcentage d'assaut gagné : ", false);
		boolean ok = false;
		while(!ok) 
		{
			if (sc.hasNextInt()) {
				int assaut = sc.nextInt();
				ok = true;
				if (assaut > 0 && assaut <= 100) {
					if (assaut < 10) {
						Disqualifier();
					} else if (assaut < 25) {
						principal.lesAthletes.get(principal.idAthlete).AjouterScore(100);
					} else if (assaut < 50) {
						principal.lesAthletes.get(principal.idAthlete).AjouterScore(200);
					} else if (assaut < 65) {
						principal.lesAthletes.get(principal.idAthlete).AjouterScore(230);
					} else {
						principal.lesAthletes.get(principal.idAthlete).AjouterScore(250);
					}
					System.out.println(principal.lesAthletes.get(principal.idAthlete).AfficheScore());
				} else {
					System.out.println("Out of range");
					ok=false;
				}
			} else {
				System.out.println("Veuillez entrer un Int");
				sc.next();
				ok=false;
			}
		}
	}

	// ------------------ FIN EPREUVE ESCRIME -------------------------------------------

	// -------------------------- NATATION   --------------------------------------------
	/**
	 * Epreuve de natation
	 * @param prenomAthlete nom de l'athlete 
	 * @param nomAthlete prenom de l'athlete
	 */
	public static void Natation(String prenomAthlete, String nomAthlete) {

		Write("Quel a été le temps de l'épreuve ? : ", false);
		int tempsEpreuve = 0;
		boolean ok = false;
		while(!ok)
		{
			if (sc.hasNextInt()) {
				tempsEpreuve = sc.nextInt();
				ok = true;
				// Verification du temps, si celui-ci est supérieur a 210 on arrête
				// le programme direct
				if (tempsEpreuve > 0 && tempsEpreuve <= 210) {
					Noyer(tempsEpreuve);
				} else {
					Disqualifier();	
				}
				System.out.println(principal.lesAthletes.get(principal.idAthlete).AfficheScore());

			} else {
				// Recall function Noyer
				System.out.println("Veuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}
	}
	/**
	 * Verifie si l'athlete s'est noye
	 * @param tempsEpreuve recupere le temps de l'epreuve pour pouvoir calculer le nombre de points
	 */
	private static void Noyer(int tempsEpreuve) {
		int tempsIdeal = 150;
		int differenceTemps;

		Write("L'athlete s'est-il noyé ? : (o/n) ", false);
		String noyer = sc.next();

		// Si l'athlete s'est noyé, il est disqualifié
		if (noyer.equals("o") || noyer.equals("O")) {
			Disqualifier();
		} else if (noyer.equals("n") || noyer.equals("N")) {
			if (tempsEpreuve > tempsIdeal) {
				differenceTemps = tempsEpreuve - tempsIdeal;
				principal.lesAthletes.get(principal.idAthlete).AjouterScore(250 - differenceTemps);
			} else if (tempsEpreuve == tempsIdeal) {
				principal.lesAthletes.get(principal.idAthlete).AjouterScore(250);
			} else {
				differenceTemps = tempsIdeal - tempsEpreuve;
				principal.lesAthletes.get(principal.idAthlete).AjouterScore(250 + differenceTemps);
			}
		}
	}

	// --------------- FIN EPREUVE NATATION --------------------------------

	// ------------------ EQUITATION ---------------------------------------
	/**
	 * Epreuve d'equitation
	 * @param prenomAthlete nom de l'athlete 
	 * @param nomAthlete prenom de l'athlete
	 */
	public static void Equitation(String prenomAthlete, String nomAthlete) {
		Write("Combien de barre l'athlete a-t-il fait tomber ? : ", false);
		boolean ok = false;
		while(!ok)
		{
			if (sc.hasNextInt()) {
				ok = true;
				int nbBarreAthlete = sc.nextInt();

				Verif(nbBarreAthlete, 12, 10);

				// On verifie si le cheval fait tomber des barres
				ChevalTomberBarre();

				// On verifie si le cheval refuse de sauter
				RefuserSauter();

				// On vérifie si l'athlete est tombé du cheval
				TomberCheval(nomAthlete, prenomAthlete);
				
				System.out.println(principal.lesAthletes.get(principal.idAthlete).AfficheScore());
			} else {
				System.out.println("Veuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}
	}
	/**
	 * Verifie si le cheval refuse de sauter la barre
	 */
	private static void RefuserSauter() {
		Write("Combien de fois le cheval a-t-il refusé de sauter ? : ", false);
		boolean ok = false;
		while(!ok)
		{
			if (sc.hasNextInt()) {
				int refus = sc.nextInt();
				// on vérifie que le cheval ne refuse pas de sauter plus de 3 fois
				Verif(refus, 3, 20);
				ok = true;
			} else {
				System.out.println("Veuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}
	}
	/**
	 * Recupere le nombre de barre que le cheval a fait tomber
	 */
	private static void ChevalTomberBarre() {
		Write("Combien de barre le cheval a-t-il fait tomber ? : ", false);
		boolean ok = false;
		
		while(!ok)
		{
			if (sc.hasNextInt()) {
				int nbBarreCheval = sc.nextInt();
				ok = true;
				Verif(nbBarreCheval, 12, 10);
			} else {
				System.out.println("Veuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}
	}
	/**
	 * Verifie si l'athlete est tomber du cheval
	 * @param nom nom de l'athlete
	 * @param prenom prenom de l'athlete
	 */
	private static void TomberCheval(String nom, String prenom) {
		Write("L'athlète est-il tombé ? (o/n) : ", false);
		String tombeAthlete = sc.next();
		boolean ok = false;
		while(!ok)
		{
			if (tombeAthlete.equals("o") || tombeAthlete.equals("O")) {
				Disqualifier();
				ok = true;
			} else if (tombeAthlete.equals("n") || tombeAthlete.equals("N")) {
				ok = true;
			} else {
				Write("Vous devez choisir entre O ou N ! ", true);
				ok = false;
			}
		}

	}
	/**
	 * Verifie que la saisie de l'utilisateur ne depasse pas une certine limite puis ajoute les points a l'athlete
	 * @param nbSaisie saisie de l'utilisateur 
	 * @param limite limite definie par type d'epreuve
	 * @param nbPoints nombre de points a attribuer
	 */
	private static void Verif(int nbSaisie, int limite, int nbPoints) {
		// On vérifie que la saisie n'est pas en dehors des limites fixées par
		// l'épreuve
		boolean ok = false;
		while(!ok)
		{
			if (nbSaisie <= limite) {
				principal.lesAthletes.get(principal.idAthlete).AjouterScore(100 - nbPoints * nbSaisie); 
				ok = true;
			} else if (nbSaisie == 0) {
				principal.lesAthletes.get(principal.idAthlete).AjouterScore(100); 
				ok = true;
			} else {
				Write("Le nombre que vous avez saisi est " + nbSaisie + " superieur à " + limite + " veuillez le re-saisir :", false);
				nbSaisie = sc.nextInt();
				ok = false;
			}
		}
	}

	// --------------- FIN EPREUVE EQUITATION --------------------------------

	// --------------- COURSE / TIR A L'ARC -----------------------------------
	/**
	 * Epreuve de course / tir a l'arc
	 * @param prenomAthlete nom de l'athlete 
	 * @param nomAthlete prenom de l'athlete
	 */
	public static void Course(String prenomAthlete, String nomAthlete) {
		Collections.sort(principal.lesAthletes);
		for (Athlete ath : principal.lesAthletes)
		{
			// Compte le nombre point au total de l'athlete
			ath.CompteScore();
			System.out.println("N°" +ath.getRang()+" - l'athlete " + ath.getNom() +" "+ath.getPrenom() + " "+ ath.getNbPoints() + " points.");
		}
	}
	// --------------- FIN EPREUVE COURSE / TIR A L'ARC --------------------------------

	// Procédure utilitaires
	/**
	 * Procedure qui sert a l'affichage et determine qi l'on veut retourner a la ligne ou non
	 * @param str phrase a afficher
	 * @param aLaLigne retour a la ligne ou non
	 */
	public static void Write(String str, boolean aLaLigne) {
		if (aLaLigne == true) {
			System.out.println(str);
		} else {
			System.out.print(str);
		}

	}
}
