package Version2;
/**
 * Classe regroupant le nom et nombre de points par epreuves
 * @author Panthier Sharon
 * @version 2
 * 
 */
public class Epreuve {
	private int num = 0;
	private String libelle;
	private int points = 0;

	/**
	 * Constructeur par defaut
	 */
	public Epreuve() {
		setLibelle("null");
		setPoints(0);
	}
	/**
	 * Constructeur a 2 parametre
	 * @param libelle nom de l'épreuve
	 * @param points nombre de points a attribuer
	 */
	public Epreuve(String libelle, int points) {
		this.libelle = libelle;
		this.points = points;
		
	}
	/**
	 * Constructeur a 1 parametre
	 * @param libelle nom de l'epreuve
	 */
	public Epreuve(String libelle) {
		this.libelle = libelle;
	}
	/**
	 * Accesseur en lecture du nombre de points
	 * @return nombre de points
	 */
	public int getPoints() {
		return points;
	}
	/**
	 * Accesseur en ecriture du nombre de points
	 * @param points entier a affecter
	 */
	public void setPoints(int points) {
		this.points = points;
	}
	/**
	 * Accesseur en lecture du nom de l'epreuve
	 * @return string nom de l'épreuve
	 */
	public String getLibelle() {
		return libelle;
	}
	/**
	 * Accesseur en ecriture au nom de l'epreuve
	 * @param libelle string nom de l'épreuve
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	/**
	 * Affichage du nom de l'epreuve et du nombre de points
	 */
	public String toString(){
		return "Epreuve  " + libelle;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	 
}
