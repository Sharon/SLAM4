package Version1;

import java.util.Scanner;
/**
 * Description : Projet penthatlon moderne
 * @author Panthier Sharon
 * @version 2
 */
public class pentathlon_moderne {

	/**
	 * Variables globales
	 */
	// Vaversriables globales
	static Scanner sc = new Scanner(System.in);
	private static int[] resultat = new int[4];
	private static int compteur = 0;
	static boolean disqualifier = false;
	static boolean abandon = false;
	private static int nbPoints = 0;

	// Vérification de l'abandon ou non de l'athlete

	public static boolean Abandon() throws Exception
	{
		boolean ok = false;
		while(!ok)
		{
			String decision;

			System.out.print("Voulez-vous abandonner ? (O/N) : ");
			decision = sc.next();
				
			if(decision.equals("o") || decision.equals("O")) {
				System.out.println("Vous avez abandonner ! ");
				abandon = true;
				ok = true;
			}
			else if (decision.equals("n") || decision.equals("N")) {
				compteur++;
				ok = true;
			} else {
				System.out.println("La saisie doit obligatoirement être 'O' ou 'N'");
				ok = false;
			}
		}
		return abandon;
	}
	public static boolean Disqualifier()
	{
		
		Write("Vous êtes discalifié ! ", true);
		
		return disqualifier = true;
	}
	// ********************* EPREUVES ************************
	
	// --------------- ESCRIME --------------------------------
	public static void Escrime(String prenomAthlete,String nomAthlete)
	{
		boolean ok = false;
		while(!ok)
		{
			Write("Veuillez saisir le pourcentage d'assaut gagné : ", false);

			if (sc.hasNextInt()) {
				int assaut  = sc.nextInt();

				if (assaut > 0 && assaut < 100) {
					if (assaut < 10) {
						Disqualifier();
					}
					else if(assaut < 25 ) {
						resultat[compteur] = 100;
					}
					else if (assaut < 50) {
						resultat[compteur] = 200;
					}
					else if (assaut < 65) {
						resultat[compteur] = 230;
					} else {
						resultat[compteur] = 250;
					}
					
					if(!disqualifier) {
						ok = true;
						Write("Fin de l'epreuve de Esc  "+nomAthlete + " "+ prenomAthlete+" a "+ resultat[compteur]+" points ", true);
					}
					else {
						ok = true;
						CompteNbPoints(compteur);
						Write("Fin de l'epreuve de Esc  "+nomAthlete + " "+ prenomAthlete+" a "+ nbPoints +" points au total ", true);
					}
				} else {
					System.out.println("Out of range");
					ok = false;
				}
			} else {
				System.out.println("Veuillez entrer un Int");
				sc.next();
				ok = false;
			}	
		}
	}
	// ------------------ FIN EPREUVE ESCRIME -------------------------------------------
	
	// -------------------------- NATATION ----------------------------------------------
	public static void Natation(String prenomAthlete,String nomAthlete)
	{
		boolean ok = false;
		while(!ok)
		{
			Write("Quel a été le temps de l'épreuve ? : ", false);
			int tempsEpreuve = 0;
			
			if (sc.hasNextInt()) {
				 tempsEpreuve = sc.nextInt();
				// Verification du temps, si celui-ci est supérieur a 210 on arrête le programme direct
				if (tempsEpreuve <= 210 ) {
					Noyer(tempsEpreuve);
				} else {
					Disqualifier();
				}
				
				if(!disqualifier) {
					ok = true;
					Write("Fin de l'epreuve de Natation  "+nomAthlete + " "+ prenomAthlete+" a "+ resultat[compteur]+" points ", true);
				} else {
					CompteNbPoints(compteur);
					Write("Fin de l'epreuve de Natation  "+nomAthlete + " "+ prenomAthlete+" a "+ nbPoints +" points au total ", true);
				}

				} else {
					// Recall function Noyer
					System.out.println("Veuillez entrer un Int");
					sc.next();
					ok = false;
				}
		}
	}
	private static void Noyer(int tempsEpreuve)
	{
		int tempsIdeal = 150;
		int differenceTemps;
		
		Write("L'athlete s'est-il noyé ? : (o/n) ", false);
		String noyer = sc.next();
		
		// Si l'athlete s'est noyé, il est disqualifié
		if( noyer.equals("o") || noyer.equals("O")) {
			Disqualifier();
		} else if ( noyer.equals("n") || noyer.equals("N"))  {
			if(tempsEpreuve > tempsIdeal) {
				differenceTemps = tempsEpreuve - tempsIdeal;
				resultat[compteur] = 250 - differenceTemps;
			}
			else if(tempsEpreuve == tempsIdeal) {
				resultat[compteur] = 250;
			} else {
				differenceTemps =  tempsIdeal - tempsEpreuve;
				resultat[compteur] = 250 + differenceTemps;
			}
		}
	}
	// --------------- FIN EPREUVE NATATION --------------------------------
	
	// ------------------ EQUITATION ---------------------------------------
	public static void Equitation(String prenomAthlete,String nomAthlete)
	{
		boolean ok = false;
		while(!ok)
		{
			Write("Combien de barre l'athlete a-t-il fait tomber ? : ", false);
			if(sc.hasNextInt()) {
				int nbBarreAthlete = sc.nextInt();
				
				Verif(nbBarreAthlete, 12, 10);
				
				// On verifie si le cheval fait tomber des barres
				ChevalTomberBarre();
				
				//On verifie si le cheval refuse de sauter
				RefuserSauter();
				
				// On vérifie si l'athlete est tombé du cheval
				TomberCheval(nomAthlete, prenomAthlete);
				
				if(!disqualifier) {
					ok = true;
					Write("Fin de l'epreuve d'Equitation  "+nomAthlete + " "+ prenomAthlete+" a "+ resultat[compteur]+" points ", true);
				} else { 
					CompteNbPoints(compteur);
					Write("Fin de l'epreuve d'equitation  "+nomAthlete+ " "+ prenomAthlete+" a obtenu "+ nbPoints +" points au total ", true);
				}
			}
			else {
				System.out.println("Veuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}
	}
	private static void RefuserSauter()
	{
		boolean ok = false;
		while(!ok)
		{
			Write("Combien de fois le cheval a-t-il refusé de sauter ? : ", false);
			if(sc.hasNextInt()) {		
				int refus = sc.nextInt();
				// on vérifie que le cheval ne refuse pas de sauter plus de 3 fois
				Verif(refus, 3,20);
				ok = true;

			} else {
				System.out.println("Veuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}
	}
	private static void ChevalTomberBarre()
	{
		boolean ok = false;
		while(!ok)
		{
			Write("Combien de barre le cheval a-t-il fait tomber ? : ", false);
			if(sc.hasNextInt()) {
				int nbBarreCheval = sc.nextInt();
				Verif(nbBarreCheval, 12, 10);
				ok = true;
			} else {
				System.out.println("Veuillez entrer un Int");
				sc.next();
				ok = false;
			}
		}

	}
	private static void TomberCheval(String nom, String prenom)
	{
		boolean ok = false;
		while(!ok)
		{
			Write("L'athlète est-il tombé ? (o/n) : ", false);
			String tombeAthlete = sc.next();
			
			if(tombeAthlete.equals("o") || tombeAthlete.equals("O")) {
				Disqualifier();
				ok = true;
			} else if (tombeAthlete.equals("n") || tombeAthlete.equals("N")){
				ok = true;
			} else {
				Write("Vous devez choisir entre O ou N ! ", true);
				ok = false;
			}
		}

	}
	private static void Verif(int nbSaisie, int limite, int nbPoints)
	{
		boolean ok = false;
		while(!ok)
		{
			// On vérifie que la saisie n'est pas en dehors des limites fixées par l'épreuve
			if(nbSaisie <= limite ) {
				resultat[compteur] += 100 - nbPoints * nbSaisie;
				ok = true;
			} else if (nbSaisie == 0) {
				ok = true;
				resultat[compteur] += 100;
			}
			else {
				Write("Le nombre que vous avez saisi est "+nbSaisie+" donc superieur à "+ limite + " et le resultat est " +resultat[compteur]+" veuillez le re-saisir :", false);
				nbSaisie = sc.nextInt();
				ok = false;
			}
		}
	}
	// --------------- FIN EPREUVE EQUITATION  --------------------------------
	
	// --------------- COURSE / TIR A L'ARC -----------------------------------
	public static void Course(String prenomAthlete,String nomAthlete)
	{
		boolean ok = false;
		while(!ok)
		{
			CompteNbPoints(compteur);

			Write("Veuillez saisir le rang de l'athlete : ", false);
			if(sc.hasNextInt()) {
				int rang = sc.nextInt();
					// Si l'athlete part en 1er alors son score aura un décalage d'une seconde par rapport au suivant
					if (rang > 0 && rang <= 3) {
						if(rang <= 1) {
							nbPoints = nbPoints - 1;
						}
						else if(rang <= 2) {
							nbPoints = nbPoints - 2;
						}
						else if(rang <= 3) {
							nbPoints = nbPoints - 3;
						}
						ok = true;
						Write(nomAthlete + "  "+ prenomAthlete+" est arrivée n°" + rang + "\nLe nombre de points est " + nbPoints, true);
					} else {
						Write("Le rang de l'athlete doit se situer entre 1 et 3 ! ", true);
						ok = false;
					}
			} else {
				sc.next();
				Course(prenomAthlete, nomAthlete);
			}
		}
		
	}
	// Compte le nombre point au total de l'athlete 
	private static void CompteNbPoints(int numEpreuve)
	{
		for(int j = 0; j < numEpreuve; j++)
		{
			nbPoints += resultat[j];
			// on verifie que les scores soient toujours positifs
			if(nbPoints < 0) {
				nbPoints = 0;
			}
		}
	}
	// --------------- FIN EPREUVE COURSE / TIR A L'ARC --------------------------------
	
	// Procédure utilitaires
	public static void  Write(String str, boolean aLaLigne)
	{
		if(aLaLigne == true)
		{
			System.out.println(str);
		}
		else
		{
			System.out.print(str);
		}

	}
}
