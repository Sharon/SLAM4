package Version1;

import java.util.Scanner;

public class principal {
	
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) throws Exception 
	{
		String nomAthlete;
		String prenomAthlete;
	
		pentathlon_moderne.Write("\t\t\t*******PENTATHLON MODERNE*********", true);
		pentathlon_moderne.Write("Veuillez saisir le nom de l'athlete : ", false);
		nomAthlete = sc.next();
	
		pentathlon_moderne.Write("Veuillez saisir le prenom de l'athlete : ", false);
		prenomAthlete = sc.next();		
		
		AppelEpreuve("escrime", true);
		pentathlon_moderne.Escrime(prenomAthlete,nomAthlete);
		// on vérifie d'abord si l'athlete n'a pas été disqualifié
		if(!pentathlon_moderne.disqualifier) {
			if(!pentathlon_moderne.Abandon()) { // Puis on vérifie si il n'a pas abandonner
				AppelEpreuve("natation", false);
				pentathlon_moderne.Natation(prenomAthlete,nomAthlete);
				if(!pentathlon_moderne.disqualifier) {
					if(!pentathlon_moderne.Abandon()) {
						AppelEpreuve("equitation", true);
						pentathlon_moderne.Equitation(prenomAthlete,nomAthlete);
						if(!pentathlon_moderne.disqualifier) {
							if(!pentathlon_moderne.Abandon()) {
								AppelEpreuve("course / tir a l'arc", false);
								pentathlon_moderne.Course(prenomAthlete,nomAthlete);
							}
						}
					}
				}
			}
		}
	}
	private static void AppelEpreuve(String nom, boolean mettreApostrophe)
	{
		if(mettreApostrophe == false){
			pentathlon_moderne.Write("Bienvenue a l'épreuve de " + nom, true);
		} else {
			pentathlon_moderne.Write("Bienvenue a l'épreuve d'" + nom, true);
		}
		
	}

}
