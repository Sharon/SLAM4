package Version3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import Version2.Athlete;

/**
 * Classe principal du programme du projet PENTHATLON MODERNE
 * @author Panthier Sharon
 * @version 2
 * 
 */
public class principal {
	static Scanner sc = new Scanner(System.in);
	public static List<Athlete> lesAthletes = new ArrayList<>();
	public static int idAthlete;
	static String filename = "src/Version3/penthatlon.txt";
	/**
	 * Permet l'ajout de nouveaux athletes
	 */
	public static void AjouterAthlete() {
		int age;
		String nationalite;
		boolean ok = false;
		
		while(!ok)
		{
			pentathlon_moderne.Write("Athlete par défaut ? y/n: ", false);
			String defaultA = sc.next().toLowerCase();
			if(defaultA.equals("y")) {
				lesAthletes.add(new Athlete());
				ok = true;
			} else if(defaultA.equals("n")) {
				pentathlon_moderne.Write("Veuillez saisir le nom de l'athlete : ", false);
				String nomAthlete = sc.next();
			
				pentathlon_moderne.Write("Veuillez saisir le prenom de l'athlete : ", false);
				String prenomAthlete = sc.next();	
				
				age = VerifAgeAthlete();
				nationalite = VerifNationalite();
				
				lesAthletes.add(new Athlete(nomAthlete, prenomAthlete,nationalite,age));
				ok = true;
			} else {
				ok = false;
			}
		}
		System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
		pentathlon_moderne.Escrime(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
		// on vérifie d'abord si l'athlete n'a pas été disqualifié
		if(!pentathlon_moderne.disqualifier) {
			if(!pentathlon_moderne.Abandon()) { // Puis on vérifie si il n'a pas abandonner
				System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
				pentathlon_moderne.Natation(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
				if(!pentathlon_moderne.disqualifier) {
					if(!pentathlon_moderne.Abandon()) {
						System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
						pentathlon_moderne.Equitation(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
						if(!pentathlon_moderne.disqualifier) {
							if(!pentathlon_moderne.Abandon()) {
								System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
								pentathlon_moderne.Course(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
							}
						}
					}
				}
			}
		}
		if(pentathlon_moderne.disqualifier || pentathlon_moderne.abandon) {
			pentathlon_moderne.FinDuJeu();
		}
		Recommencer();			
	}
	private static String VerifNationalite() {
		boolean ok = false;
		String natio = "";
		while(!ok)
		{
			int i = 1;
			List<String> LesNationalite = new ArrayList<>();
			LesNationalite.add(0, "français");
			LesNationalite.add(1, "anglais");
			LesNationalite.add(2, "japonais");
			LesNationalite.add(3, "africain");
			LesNationalite.add(4, "chinois");
			LesNationalite.add(5, "allemand");
			LesNationalite.add(5, "russe");
			for(String str : LesNationalite) {
				System.out.println(i + " - " +str +" ");
				i++;
			}
			
			pentathlon_moderne.Write("Veuillez saisir la nationalité de l'athlete : ", false);
			
			if(sc.hasNextInt()) {
				int choix = sc.nextInt();
				ok = true;
				if(choix < (LesNationalite.size()+1)) {
					choix -= 1;
					natio = LesNationalite.get(choix);
				} else {
					ok = false;
				}

			} else {
				sc.next();
				ok = false;
			}
		}

		return natio;
	}
	private static int VerifAgeAthlete() {
		boolean ok = false;
		int age = 0;
		
		while(!ok) 
		{
			System.out.print("Veuillez saisir l'age de l'athlete : ");
				
			if(sc.hasNextInt()) {
				age = sc.nextInt();	
				if (age > 20 && age < 50) 
					ok = true;
				 else {
						System.out.println("l'athlete doit avoir entre 20 et 50 ans ");
						ok = false;
					}
				} else {
					sc.next();
					ok = false;
				}
		}
		return age;
	}
	/**
	 * Verifie si l'utilisateur veux recommencer le penthatlon
	 */
	public static void Recommencer()
	{
		boolean ok = false;
		while(!ok)
		{
			pentathlon_moderne.Write("Voulez vous ajouter un nouvel athlete ? O / N : ", false);
			String recommencer = sc.next().toLowerCase();

				if(recommencer.equals("o")) {
					ok = true;
					idAthlete++;
					pentathlon_moderne.disqualifier = false;
					pentathlon_moderne.abandon = false;
					AjouterAthlete();
				} else if (recommencer.equals("n")){
					ok = true;
					pentathlon_moderne.Write("Arrêt du programme.", true);
					pentathlon_moderne.FinDuJeu();
				} else {
					System.err.println("Erreur : Il faut choisir entre O et N.");
					ok = false;
				}
		}
	}
	public static void main(String[] args)
	{
		pentathlon_moderne.Write("\t\t\t*******PENTATHLON MODERNE*********", true);
		Lecture();
		AjouterAthlete();
	}
	public static void Lecture()
	{
		BufferedReader br;
		
		String line;
		if(filename.isEmpty()) {
			
		}
		try {
			br = new BufferedReader(new FileReader(new File(filename)));
			//org.w3c.dom.Document leDocument= dom.parse(new File("src/Version3/athletes.xml"));
			
			// Partie document texte
			while((line=br.readLine()) != null)
			{
				String[] param = line.split(","); 
				
				lesAthletes.add(new Athlete(param[0],param[1],param[2],Integer.parseInt(param[3]),Integer.parseInt(param[7])));
				idAthlete++;
			}
			if (!lesAthletes.isEmpty()) {
				System.out.println("**** Les athlètes déja présent dans la liste");
				for(Athlete ath : lesAthletes)
				{
					System.out.println(ath.toString());
				}
				System.out.println("******************************");
			}
			
			br.close();
				
		}
		catch (IOException e ){
			System.err.println("Erreur : " + e);
		}
	}
}
