package Version4;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import Version2.Athlete;


public class principalv4 {
	static Scanner sc = new Scanner(System.in);
	public static List<Athlete> lesAthletes = new ArrayList<>();
	public static int idAthlete;
	static String url = "jdbc:postgresql://postgresql.bts-malraux72.net/spanthier";
	
	public static void AjouterAthlete(java.sql.Statement query) throws SQLException {
		int age;
		String nationalite;
		boolean ok = false;
		
		while(!ok)
		{

			penthatlonv4.Write("Athlete par défaut ? y/n: ", false);
			String defaultA = sc.next().toLowerCase();
			if(defaultA.equals("y")) {
				lesAthletes.add(new Athlete());
				ok = true;
			} else if(defaultA.equals("n")) {
				penthatlonv4.Write("Veuillez saisir le nom de l'athlete : ", false);
				String nomAthlete = sc.next();
			
				penthatlonv4.Write("Veuillez saisir le prenom de l'athlete : ", false);
				String prenomAthlete = sc.next();	
				
				age = VerifAgeAthlete();
				nationalite = VerifNationalite();
				
				lesAthletes.add(new Athlete(nomAthlete, prenomAthlete,nationalite,age));
				ok = true;
			} else {
				ok = false;
			}
		}
		System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
		penthatlonv4.Escrime(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
		// on vérifie d'abord si l'athlete n'a pas été disqualifié
		if(!penthatlonv4.disqualifier) {
			if(!penthatlonv4.Abandon()) { // Puis on vérifie si il n'a pas abandonner
				System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
				penthatlonv4.Natation(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
				if(!penthatlonv4.disqualifier) {
					if(!penthatlonv4.Abandon()) {
						System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
						penthatlonv4.Equitation(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
						if(!penthatlonv4.disqualifier) {
							if(!penthatlonv4.Abandon()) {
								System.out.println(lesAthletes.get(idAthlete).AfficherEpreuve());
								penthatlonv4.Course(lesAthletes.get(idAthlete).getPrenom(),lesAthletes.get(idAthlete).getNom());
							}
						}
					}
				}
			}
		}
		if(penthatlonv4.disqualifier || penthatlonv4.abandon) {
			System.out.println("Fin du penthatlon moderne");
		}
		Recommencer(query);			
	}
	private static String VerifNationalite() {
		boolean ok = false;
		String natio = "";
		while(!ok)
		{
			int i = 1;
			List<String> LesNationalite = new ArrayList<>();
			LesNationalite.add(0, "français");
			LesNationalite.add(1, "anglais");
			LesNationalite.add(2, "japonais");
			LesNationalite.add(3, "africain");
			LesNationalite.add(4, "chinois");
			LesNationalite.add(5, "allemand");
			LesNationalite.add(5, "russe");
			for(String str : LesNationalite) {
				System.out.println(i + " - " +str +" ");
				i++;
			}
			
			penthatlonv4.Write("Veuillez saisir la nationalité de l'athlete : ", false);
			
			if(sc.hasNextInt()) {
				int choix = sc.nextInt();
				ok = true;
				if(choix < (LesNationalite.size()+1)) {
					choix -= 1;
					natio = LesNationalite.get(choix);
				} else {
					ok = false;
				}

			} else {
				sc.next();
				ok = false;
			}
		}

		return natio;
	}
	private static int VerifAgeAthlete() {
		boolean ok = false;
		int age = 0;
		
		while(!ok) 
		{
			System.out.print("Veuillez saisir l'age de l'athlete : ");
				
			if(sc.hasNextInt()) {
				age = sc.nextInt();	
				if (age > 20 && age < 50) 
					ok = true;
				 else {
						System.out.println("l'athlete doit avoir entre 20 et 50 ans ");
						ok = false;
					}
				} else {
					sc.next();
					ok = false;
				}
		}
		return age;
	}
	/**
	 * Verifie si l'utilisateur veux recommencer le penthatlon
	 * @throws SQLException 
	 */
	public static void Recommencer(java.sql.Statement query) throws SQLException
	{
		boolean ok = false;
		while(!ok)
		{
			penthatlonv4.Write("Voulez vous ajouter un nouvel athlete ? O / N : ", false);
			String recommencer = sc.next().toLowerCase();

				if(recommencer.equals("o")) {
					ok = true;
					idAthlete++;
					penthatlonv4.disqualifier = false;
					penthatlonv4.abandon = false;
					AjouterAthlete(query);
				} else if (recommencer.equals("n")){
					ok = true;
					penthatlonv4.Write("Arrêt du programme.", true);
					Ecriture(query);
				} else {
					System.err.println("Erreur : Il faut choisir entre O et N.");
					ok = false;
				}
		}
	}
	
	public static ResultSet Connexion(java.sql.Statement query) throws ClassNotFoundException, SQLException
	{
			Class.forName("org.postgresql.Driver");
			String sql = "select * from penthatlon.athletes";
			ResultSet curseurResultat = query.executeQuery(sql);
			return curseurResultat;
	}
	
	
	
	public static void Lecture(ResultSet rs) throws SQLException
	{				
		while(rs.next()){
			lesAthletes.add(new Athlete(rs.getString("nom"),rs.getString("prenom"),rs.getString("nationalite"), rs.getInt("age"),rs.getInt("points")));
			idAthlete++;
		}
	}
	public static void Ecriture(java.sql.Statement query) throws SQLException
	{
		query.executeUpdate("delete from penthatlon.athletes");
		int cpt = 1;
		Collections.sort(lesAthletes);
		for(Athlete ath : lesAthletes)
		{
			query.executeUpdate("insert into penthatlon.athletes values("+cpt+",'"+ath.getNom()+"','"+ath.getPrenom()+"','"+ath.getNationalite()+"',"+ath.getAge()+","+ath.getNbPoints()+");");
			cpt++;
		}
		
	}
	public static void FinProgramme(ResultSet rs, java.sql.Statement query) throws SQLException {
		rs.close();
		query.close();
	}
	//------------- PROGRAMME PRINCIPAL ----------------------
	
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Connection maConnexion = DriverManager.getConnection(url,"s.panthier", "P@ssword");
		java.sql.Statement laRequete = maConnexion.createStatement();
		
		ResultSet rs = Connexion(laRequete);
		
		Lecture(rs);
		
		AjouterAthlete(laRequete);
		
		FinProgramme(rs, laRequete);
		
	}
}
